package com.dyggyty.morgana.plugins.document.ui.response;

import org.primefaces.model.LazyDataModel;

public class DocumentListFunctionResponse extends DocumentPluginFunctionResponse {

    private LazyDataModel documentsModel;

    public DocumentListFunctionResponse(String view) {
        super(view);
    }

    public LazyDataModel getDocumentsModel() {
        return documentsModel;
    }

    public void setDocumentsModel(LazyDataModel documentsModel) {
        this.documentsModel = documentsModel;
    }
}
