package com.dyggyty.morgana.plugins.document;

import com.dyggyty.morgana.dto.EntityDescriptorDto;
import com.dyggyty.morgana.framework.EntityDefaultFunction;
import com.dyggyty.morgana.framework.MorganaRequest;
import com.dyggyty.morgana.persistence.EntityCrudRepository;
import com.dyggyty.morgana.plugins.document.ui.response.DocumentListFunctionResponse;
import com.dyggyty.morgana.plugins.document.ui.response.DocumentPluginFunctionResponse;
import org.apache.commons.lang.StringUtils;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Map;

@SuppressWarnings("SpringJavaAutowiringInspection")
@EntityDefaultFunction(title = "action.list")
public class DocumentListFunction extends GenericDocumentPluginFunction {

    public static final String FUNCTION_NAME = "document-list";
    private static final String PAGE = "document-manager-list";

    @Override
    protected DocumentPluginFunctionResponse execute(EntityDescriptorDto entityDescriptor, MorganaRequest functionRequest) {

        DocumentListFunctionResponse response = new DocumentListFunctionResponse(PAGE);

        String entityName = entityDescriptor.getEntityName();
        EntityCrudRepository entityCrudRepository =
                getEntityDescriptorService().getCrudRepository(entityName);

        LazyDataModel model = new LazyDataModel<Object>() {
            @Override
            public List<Object> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                Sort.Direction sortDirection = sortOrder == null || SortOrder.UNSORTED.equals(sortOrder) ? null :
                        (SortOrder.ASCENDING.equals(sortOrder)) ? Sort.Direction.ASC : Sort.Direction.DESC;

                Pageable pageable;
                if (StringUtils.isEmpty(sortField)) {
                    pageable = new PageRequest(first, pageSize);
                } else {
                    pageable = new PageRequest(first, pageSize, sortDirection, sortField);
                }

                Page<Object> entitiesPage = entityCrudRepository.findAll(pageable);

                List<Object> result = entitiesPage.getContent();
                setRowCount((int) entitiesPage.getTotalElements());
                return result;
            }
        };

        response.setDocumentsModel(model);

        return response;
    }

    @Override
    protected boolean isListFunction() {
        return true;
    }

    @Override
    public String getFunctionName() {
        return FUNCTION_NAME;
    }
}
