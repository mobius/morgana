package com.dyggyty.morgana.plugins.document.ui.response;

import com.dyggyty.morgana.framework.function.EntityProviderFunctionResponse;
import com.dyggyty.morgana.plugins.document.ui.DocumentEditForm;

public class SingleDocumentFunctionResponse extends DocumentPluginFunctionResponse implements EntityProviderFunctionResponse {

    private DocumentEditForm documentEditForm;

    public SingleDocumentFunctionResponse(String view) {
        super(view);
    }

    public void setDocumentEditForm(DocumentEditForm documentEditForm) {
        this.documentEditForm = documentEditForm;
    }

    @Override
    public DocumentEditForm getEntityForm() {
        return documentEditForm;
    }
}
