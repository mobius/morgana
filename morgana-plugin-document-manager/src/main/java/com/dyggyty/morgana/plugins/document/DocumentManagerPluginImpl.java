package com.dyggyty.morgana.plugins.document;

import com.dyggyty.morgana.dto.CategoryDto;
import com.dyggyty.morgana.dto.CollectionFieldDto;
import com.dyggyty.morgana.dto.EntityDescriptorDto;
import com.dyggyty.morgana.entity.EntityDescriptorProvider;
import com.dyggyty.morgana.framework.function.PluginFunction;
import com.dyggyty.morgana.persistence.EntityCrudRepository;
import com.dyggyty.morgana.plugins.document.ui.FieldDescriptorComposite;
import com.dyggyty.morgana.plugins.document.ui.SelectOneMenuEntityComposite;
import com.dyggyty.morgana.plugins.document.utils.DocumentUtils;
import com.dyggyty.morgana.service.EntityDescriptorService;
import com.dyggyty.morgana.utils.ReflectionUtils;
import com.dyggyty.morgana.web.utils.HttpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@ManagedBean(name = DocumentManagerPluginImpl.DOCUMENT_MANGER_PLUGIN)
@RequestScoped
@Component(DocumentManagerPluginImpl.DOCUMENT_MANGER_PLUGIN)
public class DocumentManagerPluginImpl implements DocumentManagerPlugin {

    public static final String DOCUMENT_MANGER_PLUGIN = "document-manager";

    @Autowired(required = false)
    private Set<EntityDescriptorProvider> categorizedEntitiesProviders;

    @Autowired
    private EntityDescriptorService entityDescriptorService;
    @Autowired
    private MessageSource messages;

    public List<SelectOneMenuEntityComposite> getEntityCompositeForType(String entityType) {
        EntityDescriptorDto entityDescriptorDto = entityDescriptorService.getEntityDescriptor(entityType);
        EntityCrudRepository entityCrudRepository = entityDescriptorService.getCrudRepository(entityType);
        List entities = entityCrudRepository.findAll();

        List<SelectOneMenuEntityComposite> entityComposites = new ArrayList<>(entities.size());
        for (Object entity : entities) {
            String entityName = entity.toString();
            SelectOneMenuEntityComposite oneMenuItem = new SelectOneMenuEntityComposite(entity, entityName);
            entityComposites.add(oneMenuItem);
        }

        return entityComposites;
    }

    @Override
    public Set<CategoryDto> getCategories() {

        final Set<CategoryDto> categoryDtos = new HashSet<>();
        final Map<CategoryDto, List<CategoryDto>> categoryDtoMap = new HashMap<>();

        Set<EntityDescriptorDto> entityDescriptorDtos = new HashSet<>();
        if (categorizedEntitiesProviders != null) {
            categorizedEntitiesProviders.forEach(entityCrudRepository -> {
                Set<EntityDescriptorDto> entityDescriptors = entityCrudRepository.getEntityDescriptors();

                entityDescriptors.forEach(entityDescriptorDto -> {
                    if (!StringUtils.isEmpty(entityDescriptorDto.getCategoryName())) {
                        entityDescriptorDtos.add(entityDescriptorDto);
                    }
                });
            });
        }

        entityDescriptorDtos.forEach(entityDescriptorDto -> {
            CategoryDto parentCategory = entityDescriptorDto.getParentalCategory();
            List<CategoryDto> children = categoryDtoMap.get(parentCategory);
            if (children == null) {
                children = new ArrayList<>();
                categoryDtoMap.put(parentCategory, children);
                parentCategory.setChildren(children);
                categoryDtos.add(parentCategory);
            }

            CategoryDto childCategory = new CategoryDto();
            childCategory.setName(entityDescriptorDto.getCategoryName());
            childCategory.setOrder(entityDescriptorDto.getOrder());

            String entityName = entityDescriptorDto.getEntityName();
            childCategory.setPluginName(DOCUMENT_MANGER_PLUGIN);
            childCategory.setPluginFunctionName(DocumentListFunction.FUNCTION_NAME);

            Map<String, String> functionArguments = new HashMap<>();
            functionArguments.put(DocumentListFunction.PARAM_DOCUMENT_TYPE, entityName);

            childCategory.setArguments(functionArguments);

            children.add(childCategory);
        });

        return categoryDtos;
    }

    @Override
    public List<Class<? extends PluginFunction>> getPluginFunctions() {
        List<Class<? extends PluginFunction>> pluginFunctions = new LinkedList<>();
        pluginFunctions.add(DocumentListFunction.class);
        pluginFunctions.add(OpenDocumentFunction.class);
        pluginFunctions.add(NewDocumentFunction.class);
        pluginFunctions.add(SaveDocumentFunction.class);
        pluginFunctions.add(LinkReferenceFunction.class);
        pluginFunctions.add(RefreshDocumentListFunction.class);
        return pluginFunctions;
    }

    @Override
    public String getName() {
        return DOCUMENT_MANGER_PLUGIN;
    }

    public List<FieldDescriptorComposite> getFieldDescriptorCompositesForList(String entityType) {
        EntityDescriptorDto entityDescriptorDto = entityDescriptorService.getEntityDescriptor(entityType);
        return DocumentUtils.getFieldDescriptorComposites(entityDescriptorDto, HttpUtils.getBrowserLocale(), messages, true);
    }

    @Override
    @Transactional
    public Collection getAttachedObjects(Object entity, String fieldName) {

        if (entity == null) {
            return Collections.emptyList();
        }

        EntityDescriptorDto entityDescriptorDto = entityDescriptorService.getEntityDescriptor(entity.getClass());
        CollectionFieldDto currentField = ReflectionUtils.getDocumentTabFieldDto(entityDescriptorDto, fieldName);

        if (currentField == null) {
            return Collections.emptyList();
        }

        Collection attachedEntities = ReflectionUtils.getPoorValue(currentField, entity);

        return attachedEntities == null ? Collections.emptyList() : attachedEntities;
    }
}