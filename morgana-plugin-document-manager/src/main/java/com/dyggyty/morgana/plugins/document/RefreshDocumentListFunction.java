package com.dyggyty.morgana.plugins.document;

import com.dyggyty.morgana.dto.EntityDescriptorDto;
import com.dyggyty.morgana.framework.EntityDefaultFunction;
import com.dyggyty.morgana.framework.MorganaRequest;
import com.dyggyty.morgana.plugins.document.ui.response.DocumentPluginFunctionResponse;

@EntityDefaultFunction(title = "action.refresh")
public class RefreshDocumentListFunction extends GenericDocumentPluginFunction {

    public static final String FUNCTION_NAME = "document-refresh";

    @Override
    public String getFunctionName() {
        return FUNCTION_NAME;
    }

    @Override
    public <T> boolean isDefaultOnEntityList(Class<T> entityClass) {
        return true;
    }

    @Override
    protected DocumentPluginFunctionResponse execute(EntityDescriptorDto entityDescriptor, MorganaRequest functionRequest) throws Exception {
        return new DocumentPluginFunctionResponse() {
        };
    }

    @Override
    protected boolean isListFunction() {
        return true;
    }
}
