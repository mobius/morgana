package com.dyggyty.morgana.plugins.document;

import com.dyggyty.morgana.dto.CollectionFieldDto;
import com.dyggyty.morgana.dto.EntityDescriptorDto;
import com.dyggyty.morgana.entity.EntityExtender;
import com.dyggyty.morgana.framework.MorganaRequest;
import com.dyggyty.morgana.persistence.EntityCrudRepository;
import com.dyggyty.morgana.plugins.document.ui.DocumentEditForm;
import com.dyggyty.morgana.plugins.document.ui.SelectOneMenuEntityComposite;
import com.dyggyty.morgana.plugins.document.ui.response.DocumentPluginFunctionResponse;
import com.dyggyty.morgana.plugins.document.ui.response.SingleDocumentFunctionResponse;
import com.dyggyty.morgana.service.EntityDescriptorService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class SingleDocumentFunction extends GenericDocumentPluginFunction {

    public static final String PARAM_ENTITY_ID = "entityId";

    public static final String PAGE = "document-manager-edit";

    @Autowired
    private EntityDescriptorService entityDescriptorService;
    @Autowired(required = false)
    private Set<EntityExtender> entityExtenders;

    @Override
    protected DocumentPluginFunctionResponse execute(EntityDescriptorDto entityDescriptor, MorganaRequest functionRequest) {

        SingleDocumentFunctionResponse response = new SingleDocumentFunctionResponse(PAGE);

        if (entityDescriptor != null) {

            String entityName = entityDescriptor.getEntityName();

            EntityCrudRepository entityCrudRepository =
                    entityDescriptorService.getCrudRepository(entityName);
            Object entity;

            final Class entityClass = entityDescriptor.getEntityClass();
            if (functionRequest.containsKey(PARAM_ENTITY_ID)) {
                String entityId = ((String[]) functionRequest.getParameter(PARAM_ENTITY_ID))[0];
                entity = entityCrudRepository.findOne(entityId);
            } else {
                try {
                    entity = entityClass.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                    throw new InstantiationError("Can't create instance of the "
                            + entityDescriptor.getEntityName() + " entity");
                }
            }

            DocumentEditForm form = new DocumentEditForm();
            form.setEntity(entity);
            form.setEntitiesToLink(getEntitiesToAttach(entityDescriptor));
            form.setLinkedEntities(getAttachedEntities(entityDescriptor, entity));
            response.setDocumentEditForm(form);

/*
            List<AttachedEntityComposite> attachedEntityComposites = new ArrayList<>();

            if (entityId != null && entityExtenders != null) {
                entityExtenders.forEach(entityExtender -> {
                    AttachedEntityDescriptorDto attachedEntityDescriptorDto =
                            entityExtender.getAttacheEntityDescriptor(entityClass);
                    List<LinkedEntity> attachedEntities = entityExtender.getLinkedEntities(entityId, entityName);
                    if (attachedEntities != null) {
                        attachedEntities.forEach(attachedEntity -> {
                            List<DocumentFieldValueDto> documentFieldValueDtos =
                                    EntityUtils.getDocumentFieldValueDtos(attachedEntityDescriptorDto, entity);

                            AttachedEntityComposite attachedEntityComposite =
                                    new AttachedEntityComposite(attachedEntityDescriptorDto, documentFieldValueDtos);

                            attachedEntityComposites.add(attachedEntityComposite);
                        });
                    }
                });
            }

            EntityComposite entityComposite = new EntityComposite(entityDescriptor, documentFieldValueDtos);
            entityComposite.setLinkedEntities(attachedEntityComposites);

            responseArguments.put(ENTITY, entityComposite);
*/
        }

        return response;
    }

    @Override
    protected boolean isListFunction() {
        return false;
    }

    private Map<String, Collection<SelectOneMenuEntityComposite>> getEntitiesToAttach(EntityDescriptorDto entityDescriptor) {

        Map<String, Collection<SelectOneMenuEntityComposite>> selectMenuItems = new HashMap<>();

        List<CollectionFieldDto> documentFieldTabs = entityDescriptor.getDocumentFieldTabs();
        DocumentManagerPlugin plugin = (DocumentManagerPlugin) getPlugin();
        for (CollectionFieldDto documentTab : documentFieldTabs) {
            Collection<SelectOneMenuEntityComposite> entitiesToAttache =
                    plugin.getEntityCompositeForType(documentTab.getBindableType());
            selectMenuItems.put(documentTab.getName(), entitiesToAttache);
        }

        return selectMenuItems;
    }

    private Map<String, Collection<DocumentEditForm.LinkedEntity>> getAttachedEntities(EntityDescriptorDto entityDescriptor, Object entity) {

        Map<String, Collection<DocumentEditForm.LinkedEntity>> attachedEntities = new HashMap<>();

        List<CollectionFieldDto> documentFieldTabs = entityDescriptor.getDocumentFieldTabs();
        final DocumentManagerPlugin plugin = (DocumentManagerPlugin) getPlugin();
        for (CollectionFieldDto documentTab : documentFieldTabs) {
            Collection fieldEntities = plugin.getAttachedObjects(entity, documentTab.getName());
            List<DocumentEditForm.LinkedEntity> entities = new ArrayList<>();
            for (Object fieldEntity : fieldEntities) {
                DocumentEditForm.LinkedEntity linkedEntity = new DocumentEditForm.LinkedEntity(fieldEntity);
                entities.add(linkedEntity);
            }
            attachedEntities.put(documentTab.getName(), entities);
        }

        return attachedEntities;

    }
}
