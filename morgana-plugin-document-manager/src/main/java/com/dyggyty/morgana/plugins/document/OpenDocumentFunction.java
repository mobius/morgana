package com.dyggyty.morgana.plugins.document;

import com.dyggyty.morgana.framework.EntityDefaultFunction;

/**
 * Open existing document.
 */
@EntityDefaultFunction(title = "action.edit")
public class OpenDocumentFunction extends SingleDocumentFunction {
    public static final String FUNCTION_NAME = "open-document";

    @Override
    public String getFunctionName() {
        return FUNCTION_NAME;
    }
}
