package com.dyggyty.morgana.plugins.document;

import com.dyggyty.morgana.dto.CollectionFieldDto;
import com.dyggyty.morgana.dto.EntityActionDto;
import com.dyggyty.morgana.dto.EntityDescriptorDto;
import com.dyggyty.morgana.framework.MorganaRequest;
import com.dyggyty.morgana.framework.function.FunctionResponse;
import com.dyggyty.morgana.framework.function.GenericPluginFunction;
import com.dyggyty.morgana.plugins.document.ui.FieldDescriptorComposite;
import com.dyggyty.morgana.plugins.document.ui.TabbedFieldDescriptorComposite;
import com.dyggyty.morgana.plugins.document.ui.response.DocumentPluginFunctionResponse;
import com.dyggyty.morgana.plugins.document.utils.DocumentUtils;
import com.dyggyty.morgana.service.EntityDescriptorService;
import com.dyggyty.morgana.web.component.PluginProviderComponent;
import com.dyggyty.morgana.web.utils.HttpUtils;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuItem;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Document plugin function with common operations.
 */
public abstract class GenericDocumentPluginFunction extends GenericPluginFunction {

    public static final String PARAM_DOCUMENT_TYPE = "documentType";

    @Autowired
    private EntityDescriptorService entityDescriptorService;
    @Autowired
    private MessageSource messages;

    @Transactional
    @Override
    public FunctionResponse execute(MorganaRequest functionRequest) throws Exception {
        Map<String, String[]> request = functionRequest.getParameters();

        //Checking required arguments
        if (!request.containsKey(PARAM_DOCUMENT_TYPE)) {
            throw new IllegalArgumentException("Missing '" + PARAM_DOCUMENT_TYPE + "' parameter.");
        }

        String requestedEntityName = ((String[]) functionRequest.getParameter(PARAM_DOCUMENT_TYPE))[0];

        EntityDescriptorDto entityDescriptor = entityDescriptorService.getEntityDescriptor(requestedEntityName);

        if (entityDescriptor == null) {
            throw new IllegalStateException("Can't find descriptor for the " + requestedEntityName + " entity");
        }

        DocumentPluginFunctionResponse functionResponse = execute(entityDescriptor, functionRequest);

        Locale browserLocale = HttpUtils.getBrowserLocale();

        String listFormTitle = entityDescriptor.getListFormTitle();
        listFormTitle = messages.getMessage(listFormTitle, null, listFormTitle, browserLocale);
        functionResponse.setEntityListTitle(listFormTitle);

        String entityTitle = entityDescriptor.getEntityTitle();
        entityTitle = messages.getMessage(entityTitle, null, entityTitle, browserLocale);
        functionResponse.setEntityTitle(entityTitle);

        List<FieldDescriptorComposite> fieldDescriptorComposites
                = DocumentUtils.getFieldDescriptorComposites(entityDescriptor, HttpUtils.getBrowserLocale(), messages, isListFunction());
        functionResponse.setDocumentFields(fieldDescriptorComposites);
        functionResponse.setDocumentFieldTabs(getFieldTabsDescriptorCompositeList(entityDescriptor));
        functionResponse.setEntityName(entityDescriptor.getEntityName());

        if (entityDescriptor.getEntityActionDtos() != null) {
            functionResponse.
                    setToolbarMenuModel(createMenuModel(entityDescriptor.getEntityActionDtos(), requestedEntityName));
        }

        return functionResponse;
    }

    /**
     * Execute function.
     *
     * @param entityDescriptor Requested entity descriptor.
     * @param functionRequest  function request.
     * @return Result of the function execution.
     */
    protected abstract DocumentPluginFunctionResponse execute(EntityDescriptorDto entityDescriptor, MorganaRequest functionRequest)
            throws Exception;

    protected EntityDescriptorService getEntityDescriptorService() {
        return entityDescriptorService;
    }

    /**
     * Indicates that function displays list of entities.
     *
     * @return <b>TRUE</b> is function display list of entities or <b>FALSE</b> otherwise.
     */
    protected abstract boolean isListFunction();

    protected List<TabbedFieldDescriptorComposite> getFieldTabsDescriptorCompositeList(EntityDescriptorDto entityDescriptor) {
        List<TabbedFieldDescriptorComposite> response;
        if (isListFunction() || entityDescriptor.getDocumentFields() == null || entityDescriptor.getDocumentFields().size() == 0) {
            response = Collections.emptyList();
        } else {
            response = new ArrayList<>();

            Locale browserLocale = HttpUtils.getBrowserLocale();

            List<CollectionFieldDto> documentFieldTabs = entityDescriptor.getDocumentFieldTabs();
            documentFieldTabs.stream().forEach(documentField -> {

                EntityDescriptorDto entityDescriptorDto =
                        entityDescriptorService.getEntityDescriptor(documentField.getBindableType());
                String title = entityDescriptorDto.getListFormTitle();
                title = messages.getMessage(title, new Object[]{}, title, browserLocale);

                TabbedFieldDescriptorComposite fieldDescriptor =
                        new TabbedFieldDescriptorComposite(title, documentField.getName(),
                                documentField.getBindableType());

                response.add(fieldDescriptor);
            });
        }

        return Collections.unmodifiableList(response);
    }

    private MenuModel createMenuModel(List<EntityActionDto> entityActionDtos, String requestedEntityName) {
        Locale browserLocale = HttpUtils.getBrowserLocale();

        List<MenuItem> menuItems = new ArrayList<>();
        //int menuIndex = 0;
        for (EntityActionDto entityActionDto : entityActionDtos) {
            if ((isListFunction() && entityActionDto.isVisibleOnList()) || (!isListFunction() && entityActionDto.isVisibleOnForm())) {

                String childMenuTitle = entityActionDto.getTitle();
                String menuItemTitle =
                        messages.getMessage(childMenuTitle, new Object[]{}, childMenuTitle, browserLocale);
                DefaultMenuItem menuItem = new DefaultMenuItem(menuItemTitle);
                menuItem.setTitle(menuItemTitle);
                menuItem.setAjax(false);
                menuItem.setIcon(entityActionDto.getImage());

                menuItem.setParam(HttpUtils.PLUGIN_NAME, entityActionDto.getPluginName());
                menuItem.setParam(HttpUtils.FUNCTION_NAME, entityActionDto.getFunctionName());
                menuItem.setParam(PARAM_DOCUMENT_TYPE, requestedEntityName);

                menuItem.setCommand(PluginProviderComponent.FUNCTION_CALL_COMMAND);

                menuItems.add(menuItem);
            }
        }

        MenuModel menuModel;
        if (menuItems.size() > 0) {
            menuModel = new DefaultMenuModel();
            menuItems.forEach(menuModel::addElement);
            menuModel.generateUniqueIds();
        } else {
            menuModel = null;
        }
        return menuModel;
    }
}
