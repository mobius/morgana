package com.dyggyty.morgana.plugins.document;

import com.dyggyty.morgana.dto.CollectionFieldDto;
import com.dyggyty.morgana.dto.EntityDescriptorDto;
import com.dyggyty.morgana.framework.MorganaRequest;
import com.dyggyty.morgana.framework.function.FunctionResponse;
import com.dyggyty.morgana.framework.function.GenericPluginFunction;
import com.dyggyty.morgana.plugins.document.ui.DocumentEditForm;
import com.dyggyty.morgana.plugins.document.ui.response.DocumentPluginFunctionResponse;
import com.dyggyty.morgana.service.EntityDescriptorService;
import com.dyggyty.morgana.utils.ReflectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class LinkReferenceFunction extends GenericPluginFunction {

    private static final String FUNCTION_NAME = "link-reference-document";
    private static final String REQUEST_FIELD_NAME = "fieldName";

    @Autowired
    private EntityDescriptorService entityDescriptorService;

    @Override
    public FunctionResponse execute(MorganaRequest functionRequest) throws Exception {
        DocumentEditForm documentEditForm = (DocumentEditForm) functionRequest.getEntityForm();
        String fieldName = functionRequest.getParameters().get(REQUEST_FIELD_NAME)[0];

        Object entity = documentEditForm.getEntity();

        EntityDescriptorDto entityDescriptorDto = entityDescriptorService.getEntityDescriptor(entity.getClass());
        CollectionFieldDto documentFieldDto = ReflectionUtils.getDocumentTabFieldDto(entityDescriptorDto, fieldName);

        DocumentEditForm.EntityToLink entityToLink = documentEditForm.getEntityToLink(fieldName);
        Object linkedEntity = entityToLink.getEntityToLink();
        setEntityToBean(documentFieldDto.getMember(), entity, linkedEntity);

        DocumentEditForm.LinkedEntity documentLinkedEntity = new DocumentEditForm.LinkedEntity(linkedEntity);
        if (documentFieldDto.getReferencedField() != null) {
            setEntityToBean(documentFieldDto.getReferencedField(), linkedEntity, entity);
            documentLinkedEntity.setRequiredToSave(true);
        }

        documentEditForm.getLinkedEntities(fieldName).add(documentLinkedEntity);

        return new DocumentPluginFunctionResponse() {
        };
    }

    @Override
    public String getFunctionName() {
        return FUNCTION_NAME;
    }

    private void setEntityToBean(Member member, Object mainEntity, Object linkedEntity) {

        Class<?> fieldType;
        if (member instanceof Field) {
            fieldType = ((Field) member).getType();
        } else {
            fieldType = ((Method) member).getReturnType();
        }

        if (fieldType.isAssignableFrom(linkedEntity.getClass())) {
            ReflectionUtils.setPoorValue(member, mainEntity, linkedEntity);
        } else if (Collection.class.isAssignableFrom(fieldType)) {

            Collection fieldEntities = ReflectionUtils.getPoorValue(member, mainEntity);
            if (fieldEntities == null) {
                if (Set.class.isAssignableFrom(fieldType)) {
                    fieldEntities = new HashSet<>();
                } else {
                    fieldEntities = new ArrayList<>();
                }

                ReflectionUtils.setPoorValue(member, mainEntity, fieldEntities);
            }

            fieldEntities.add(linkedEntity);
        }
    }
}
