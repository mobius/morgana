package com.dyggyty.morgana.plugins.document;

import com.dyggyty.morgana.framework.EntityDefaultFunction;

/**
 * Creates new document.
 */
@EntityDefaultFunction(title = "action.new", image = "ui-icon-plus")
public class NewDocumentFunction extends SingleDocumentFunction {

    public static final String FUNCTION_NAME = "new-document";

    @Override
    public String getFunctionName() {
        return FUNCTION_NAME;
    }


    @Override
    public <T> boolean isDefaultOnEntityList(Class<T> entityClass) {
        return true;
    }
}
