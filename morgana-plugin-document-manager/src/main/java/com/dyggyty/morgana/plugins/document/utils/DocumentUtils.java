package com.dyggyty.morgana.plugins.document.utils;

import com.dyggyty.morgana.dto.DocumentFieldDto;
import com.dyggyty.morgana.dto.EntityDescriptorDto;
import com.dyggyty.morgana.entity.FieldTemplate;
import com.dyggyty.morgana.plugins.document.ui.FieldDescriptorComposite;
import com.dyggyty.morgana.plugins.document.ui.ReferenceFieldDescriptorComposite;
import org.springframework.context.MessageSource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class DocumentUtils {

    private DocumentUtils() {
        //do nothing here
    }

    /**
     * Created field descriptor composites from entity descriptor.
     *
     * @param entityDescriptor Entity Descriptor DTO.
     * @param browserLocale    Browser locale.
     * @param messages         Message bundle.
     * @param isList           <b>TRUE</b> if fields descriptors for the table of <b>FALSE</b> otherwise.
     * @return List of fields descriptors.
     */
    public static List<FieldDescriptorComposite> getFieldDescriptorComposites(EntityDescriptorDto entityDescriptor, Locale browserLocale, MessageSource messages, boolean isList) {
        List<FieldDescriptorComposite> response;
        if (entityDescriptor.getDocumentFields() == null || entityDescriptor.getDocumentFields().size() == 0) {
            response = Collections.emptyList();
        } else {
            response = new ArrayList<>();

            List<DocumentFieldDto> documentFields = entityDescriptor.getDocumentFields();
            documentFields.forEach(documentField -> {
                if ((isList && !documentField.isHideOnTable()) ||
                        (!isList && !documentField.isHideOnForm())) {
                    String title = documentField.getTitle();
                    title = messages.getMessage(title, new Object[]{}, title, browserLocale);

                    //Retrieving field template for the list of entities
                    boolean idFlag = documentField.isId();
                    String fieldTemplate = getTemplateName(documentField);


                    FieldDescriptorComposite fieldDescriptor;

                    if (FieldTemplate.REFERENCE.equals(documentField.getTemplate())) {
                        fieldDescriptor = new ReferenceFieldDescriptorComposite(title, documentField.getName(), documentField.getBindableType());
                    } else {
                        fieldDescriptor = new FieldDescriptorComposite(title, documentField.getName(), fieldTemplate, idFlag);
                    }
                    response.add(fieldDescriptor);
                }
            });

        }

        return Collections.unmodifiableList(response);
    }

    private static String getTemplateName(DocumentFieldDto documentField) {
        boolean idFlag = documentField.isId();
        FieldTemplate fieldTemplate = documentField.getTemplate();

        String templateName = null;
        if (idFlag) {
            templateName = FieldTemplate.ID.getName();
        } else if (fieldTemplate != null) {
            templateName = fieldTemplate.getName();
        }
        if (templateName == null) {
            templateName = FieldTemplate.STRING.getName();
        }

        return templateName;
    }
}
