package com.dyggyty.morgana.plugins.document.ui;

import com.dyggyty.morgana.utils.CollectionUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DocumentEditForm implements Serializable {

    private Object entity;
    private Map<String, EntityToLink> entityToLinkMap = new HashMap<>();
    private Map<String, Collection<LinkedEntity>> linkedEntities;
    private Map<String, Collection<SelectOneMenuEntityComposite>> entitiesToLink;

    public Object getEntity() {
        return entity;
    }

    public void setEntity(Object entity) {
        this.entity = entity;
    }

    public void setLinkedEntities(Map<String, Collection<LinkedEntity>> linkedEntities) {
        this.linkedEntities = linkedEntities;
    }

    public void setEntitiesToLink(Map<String, Collection<SelectOneMenuEntityComposite>> entitiesToLink) {
        this.entitiesToLink = entitiesToLink;
    }

    public Map<String, Collection<LinkedEntity>> getLinkedEntities() {
        return linkedEntities;
    }

    /**
     * Return list of attached entities by field name.
     *
     * @param fieldName Field name.
     * @return Collection of attached entities.
     */
    public Collection<LinkedEntity> getLinkedEntities(String fieldName) {

        Collection<LinkedEntity> attachedEntities = null;

        if (this.linkedEntities != null) {
            attachedEntities = this.linkedEntities.get(fieldName);
        }

        if (attachedEntities == null) {
            attachedEntities = Collections.EMPTY_LIST;
        }

        return attachedEntities;
    }

    /**
     * Return list of entities by field name to attache.
     *
     * @param fieldName Field name.
     * @return Collection of attached entities.
     */
    public Collection<SelectOneMenuEntityComposite> getEntitiesToLink(String fieldName) {

        Collection<SelectOneMenuEntityComposite> entitiesToAttach = null;

        if (this.entitiesToLink != null) {
            entitiesToAttach = this.entitiesToLink.get(fieldName);
        }

        if (entitiesToAttach == null) {
            entitiesToAttach = Collections.EMPTY_LIST;
        }

        return entitiesToAttach;
    }

    public Map<String, EntityToLink> getEntityToLinkMap() {
        return entityToLinkMap;
    }

    public EntityToLink getEntityToLink(String fieldName) {
        return CollectionUtils.getOrCreateSynchronously(entityToLinkMap, fieldName, EntityToLink::new);
    }

    public static class LinkedEntity {
        private Object entity;
        private Boolean selected = Boolean.FALSE;
        private boolean requiredToSave = false;

        public LinkedEntity(Object entity) {
            this.entity = entity;
        }

        public Object getEntity() {
            return entity;
        }

        public void setEntity(Object entity) {
            this.entity = entity;
        }

        public Boolean getSelected() {
            return selected;
        }

        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        public boolean isRequiredToSave() {
            return requiredToSave;
        }

        public void setRequiredToSave(boolean requiredToSave) {
            this.requiredToSave = requiredToSave;
        }
    }

    public static class EntityToLink {

        private Object entityToLink;

        public Object getEntityToLink() {
            return entityToLink;
        }

        public void setEntityToLink(Object entityToLink) {
            this.entityToLink = entityToLink;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (!(o instanceof EntityToLink)) return false;

            EntityToLink that = (EntityToLink) o;

            return new EqualsBuilder()
                    .append(entityToLink, that.entityToLink)
                    .isEquals();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder(17, 37)
                    .append(entityToLink)
                    .toHashCode();
        }
    }
}
