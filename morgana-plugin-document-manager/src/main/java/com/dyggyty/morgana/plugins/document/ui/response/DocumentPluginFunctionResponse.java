package com.dyggyty.morgana.plugins.document.ui.response;

import com.dyggyty.morgana.framework.function.GenericFunctionResponse;
import com.dyggyty.morgana.plugins.document.ui.FieldDescriptorComposite;
import com.dyggyty.morgana.plugins.document.ui.TabbedFieldDescriptorComposite;
import org.primefaces.model.menu.MenuModel;

import java.util.List;

public abstract class DocumentPluginFunctionResponse extends GenericFunctionResponse {

    private MenuModel toolbarMenuModel;
    private String entityListTitle;
    private String entityTitle;
    private List<FieldDescriptorComposite> documentFields;
    private List<TabbedFieldDescriptorComposite> documentFieldTabs;
    private String entityName;

    public DocumentPluginFunctionResponse(String view) {
        super(view);
    }

    public DocumentPluginFunctionResponse() {
        super();
    }

    public MenuModel getToolbarMenuModel() {
        return toolbarMenuModel;
    }

    public void setToolbarMenuModel(MenuModel toolbarMenuModel) {
        this.toolbarMenuModel = toolbarMenuModel;
    }

    public String getEntityListTitle() {
        return entityListTitle;
    }

    public void setEntityListTitle(String entityListTitle) {
        this.entityListTitle = entityListTitle;
    }

    public String getEntityTitle() {
        return entityTitle;
    }

    public void setEntityTitle(String entityTitle) {
        this.entityTitle = entityTitle;
    }

    public List<FieldDescriptorComposite> getDocumentFields() {
        return documentFields;
    }

    public void setDocumentFields(List<FieldDescriptorComposite> documentFields) {
        this.documentFields = documentFields;
    }

    public List<TabbedFieldDescriptorComposite> getDocumentFieldTabs() {
        return documentFieldTabs;
    }

    public void setDocumentFieldTabs(List<TabbedFieldDescriptorComposite> documentFieldTabs) {
        this.documentFieldTabs = documentFieldTabs;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }
}
