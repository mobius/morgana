package com.dyggyty.morgana.plugins.document;

import com.dyggyty.morgana.framework.EntityDefaultFunction;
import com.dyggyty.morgana.framework.MorganaRequest;
import com.dyggyty.morgana.framework.function.GenericPluginFunction;
import com.dyggyty.morgana.framework.function.PluginFunction;
import com.dyggyty.morgana.persistence.EntityCrudRepository;
import com.dyggyty.morgana.plugins.document.ui.DocumentEditForm;
import com.dyggyty.morgana.plugins.document.ui.response.DocumentPluginFunctionResponse;
import com.dyggyty.morgana.service.EntityDescriptorService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Save the entity.
 */
@EntityDefaultFunction(title = "action.save")
public class SaveDocumentFunction extends GenericPluginFunction implements PluginFunction {

    public static final String FUNCTION_NAME = "save-document";

    @Autowired
    private EntityDescriptorService entityDescriptorService;

    @Override
    public DocumentPluginFunctionResponse execute(MorganaRequest functionRequest) {

        String documentType = ((String[]) functionRequest.getParameter(DocumentListFunction.PARAM_DOCUMENT_TYPE))[0];
        DocumentEditForm documentEditForm = (DocumentEditForm) functionRequest.getEntityForm();

        EntityCrudRepository entityCrudRepository = entityDescriptorService.getCrudRepository(documentType);

        Collection<Collection<DocumentEditForm.LinkedEntity>> linkedEntitiesCollection =
                documentEditForm.getLinkedEntities().values();

        Collection entitiesToSave = new LinkedList<>();

        linkedEntitiesCollection.forEach(linkedEntities -> {
            linkedEntities.stream().filter(DocumentEditForm.LinkedEntity::isRequiredToSave).forEach(linkedEntity -> {
                entitiesToSave.add(linkedEntity.getEntity());
            });
        });

        entityCrudRepository.save(entitiesToSave);
        entityCrudRepository.save(documentEditForm.getEntity());

        return new DocumentPluginFunctionResponse() {
        };
    }

    @Override
    public String getFunctionName() {
        return FUNCTION_NAME;
    }

    @Override
    public <T> boolean isDefaultOnEntityForm(Class<T> entityClass) {
        return true;
    }
}
