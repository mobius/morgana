package com.dyggyty.morgana.entity;

import com.dyggyty.morgana.dto.AttachedEntityDescriptorDto;
import com.dyggyty.morgana.dto.DocumentFieldDto;
import com.dyggyty.morgana.model.flow.FlowStatusValue;
import com.dyggyty.morgana.persistence.FlowStatusValueCrudRepository;
import com.dyggyty.morgana.persistence.FlowStatusValueCrudRepositoryImpl;
import com.dyggyty.morgana.plugins.flow.FlowProcessor;
import com.dyggyty.morgana.plugins.flow.Flowable;
import com.dyggyty.morgana.plugins.flow.GenericFlowProcessor;
import com.dyggyty.morgana.service.EntityDescriptorService;
import com.dyggyty.morgana.utils.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Flow plugin entity extender
 */
public class FlowEntityExtender implements EntityExtender<FlowStatusValue> {

    private static final String ENTITY_FLOW_TITLE = "entity.flow.title";
    private static final String ENTITY_FLOW_NAME = "FlowStatusValue";

    private final Map<Class, FlowProcessor> FLOW_PROCESSORS = new WeakHashMap<>();
    private final Map<Class, AttachedEntityDescriptorDto<FlowStatusValue>>
            ENTITY_DESCRIPTORS = new HashMap<>();
    private volatile FlowStatusValueCrudRepository flowStatusValueCrudRepository;

    private static List<DocumentFieldDto> FLOW_DOCUMENT_FIELDS = new LinkedList<>();

    static {

        //TODO initialize fields
/*
        DocumentFieldDto documentFieldDto = new DocumentFieldDto(FlowStatusValue.class);
        documentFieldDto.set
        FLOW_DOCUMENT_FIELDS.add();
*/
    }

    @Autowired
    private EntityDescriptorService entityDescriptorService;

    @SuppressWarnings("unchecked")
    @Override
    public AttachedEntityDescriptorDto<FlowStatusValue> getAttacheEntityDescriptor(
            Class<?> entityClass) {
        Flowable flowable = entityClass.getAnnotation(Flowable.class);

        if (flowable == null) {
            return null;
        }
        return getOrCreateEntityDescriptor(entityClass);
    }

    @Override
    public List<FlowStatusValue> getAttachedEntities(String entityId, String entityName) {
        return getFlowStatusValueCrudRepository().getAttachedEntities(entityId, entityName);
    }

    @Override
    public List<FlowStatusValue> onEntityCreate(Object mainEntity, List<FlowStatusValue> attachedEntities) {
        FlowProcessor flowProcessor = getOrCreateProcessor(mainEntity.getClass());
        FlowStatusValue flowStatusValue =
                attachedEntities != null && attachedEntities.size() > 0 ? attachedEntities.get(0) : null;
        flowStatusValue = flowProcessor.onEntityCreate(mainEntity, flowStatusValue);

        //todo check flow path

        return CollectionUtils.listOfElements(flowStatusValue);
    }

    @Override
    public List<FlowStatusValue> onEntityUpdate(Object mainEntityBefore, Object mainEntityAfter,
                                                List<FlowStatusValue> attachedEntities) {

        if (mainEntityAfter.getClass().equals(mainEntityBefore.getClass())) {
            throw new IllegalArgumentException(
                    "Entity before update and entity after update mus be of same type");
        }

        FlowProcessor flowProcessor = getOrCreateProcessor(mainEntityBefore.getClass());
        FlowStatusValue flowStatusValue =
                attachedEntities != null && attachedEntities.size() > 0 ? attachedEntities.get(0) : null;
        flowStatusValue = flowProcessor.onEntityUpdate(mainEntityBefore, mainEntityAfter, flowStatusValue);

        //todo check flow path

        return CollectionUtils.listOfElements(flowStatusValue);
    }

    private FlowProcessor getOrCreateProcessor(Class<?> entityClass) {
        return CollectionUtils.
                getOrCreateSynchronously(FLOW_PROCESSORS, entityClass, () -> {
                    FlowProcessor flowProcessor = null;
                    Flowable flowable = entityClass.getAnnotation(Flowable.class);

                    Class<? extends FlowProcessor> flowProcessorClass = null;
                    if (flowable != null) {
                        flowProcessorClass = flowable.flowProcessor();
                    }

                    if (flowProcessorClass == null) {
                        flowProcessor = new GenericFlowProcessor();
                    } else {
                        flowProcessor = flowProcessorClass.newInstance();
                    }

                    return flowProcessor;
                });
    }

    private AttachedEntityDescriptorDto<FlowStatusValue> getOrCreateEntityDescriptor(Class<?> entityClass) {
        return CollectionUtils.
                getOrCreateSynchronously(ENTITY_DESCRIPTORS, entityClass, () -> {

                    Flowable flowable = entityClass.getAnnotation(Flowable.class);

                    AttachedEntityDescriptorDto<FlowStatusValue> entityDescriptorDto
                            = new AttachedEntityDescriptorDto<>();

                    entityDescriptorDto.setCrudRepository(FlowStatusValueCrudRepositoryImpl.class);
                    entityDescriptorDto.setDocumentFields(FLOW_DOCUMENT_FIELDS);
                    entityDescriptorDto.setMultiple(false);
                    entityDescriptorDto.setReadOnly(flowable.editable());
                    entityDescriptorDto.setEntityClass(FlowStatusValue.class);
                    entityDescriptorDto.setEntityName(ENTITY_FLOW_NAME);
                    entityDescriptorDto.setEntityTitle(ENTITY_FLOW_TITLE);

                    return entityDescriptorDto;
                });
    }

    private FlowStatusValueCrudRepository getFlowStatusValueCrudRepository() {
        if (flowStatusValueCrudRepository == null) {
            synchronized (this) {
                if (flowStatusValueCrudRepository == null) {
                    flowStatusValueCrudRepository =
                            entityDescriptorService.getCrudRepository(FlowStatusValueCrudRepositoryImpl.class, null);
                }
            }
        }

        return flowStatusValueCrudRepository;
    }
}
