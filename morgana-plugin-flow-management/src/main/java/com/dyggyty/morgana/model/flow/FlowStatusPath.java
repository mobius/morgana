package com.dyggyty.morgana.model.flow;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "flow_status_path")
public class FlowStatusPath {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private FlowStatusEntry currentStatusEntry;

    @ManyToOne
    private FlowStatusEntry nextStatusEntry;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FlowStatusEntry getCurrentStatusEntry() {
        return currentStatusEntry;
    }

    public void setCurrentStatusEntry(FlowStatusEntry currentStatusEntry) {
        this.currentStatusEntry = currentStatusEntry;
    }

    public FlowStatusEntry getNextStatusEntry() {
        return nextStatusEntry;
    }

    public void setNextStatusEntry(FlowStatusEntry nextStatusEntry) {
        this.nextStatusEntry = nextStatusEntry;
    }
}
