package com.dyggyty.morgana.model.flow;

import com.dyggyty.morgana.configuration.MenuItem;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

/**
 * Flow entity status
 */
@Entity
@Table(name = "flow_status")
@MenuItem("menu.flows")
public class FlowStatus {

    @Id
    @GeneratedValue
    private Long id;

    private String mappedEntityClass;

    @OneToMany()
    private Set<FlowStatusEntry> flowStatusEntries;

    @ManyToOne
    private FlowStatusPath flowStatusPath;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMappedEntityClass() {
        return mappedEntityClass;
    }

    public void setMappedEntityClass(String mappedEntityClass) {
        this.mappedEntityClass = mappedEntityClass;
    }

    public Set<FlowStatusEntry> getFlowStatusEntries() {
        return flowStatusEntries;
    }

    public void setFlowStatusEntries(Set<FlowStatusEntry> flowStatusEntries) {
        this.flowStatusEntries = flowStatusEntries;
    }

    public FlowStatusPath getFlowStatusPath() {
        return flowStatusPath;
    }

    public void setFlowStatusPath(FlowStatusPath flowStatusPath) {
        this.flowStatusPath = flowStatusPath;
    }
}
