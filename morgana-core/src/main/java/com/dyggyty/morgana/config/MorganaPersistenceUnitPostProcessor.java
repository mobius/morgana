package com.dyggyty.morgana.config;

import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.orm.jpa.persistenceunit.MutablePersistenceUnitInfo;
import org.springframework.orm.jpa.persistenceunit.PersistenceUnitPostProcessor;

import javax.persistence.Entity;
import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * This PersistenceUnitPostProcessor is used to search given package list for JPA
 * entities and add them as managed entities. By default the JPA engine searches
 * for persistent classes only in the same class-path of the location of the
 * persistence.xml file.  When running unit tests the entities end up in test-classes
 * folder which does not get scanned.  To avoid specifying each entity in the persistence.xml
 * file to scan, this post processor automatically adds the entities for you.
 */
public class MorganaPersistenceUnitPostProcessor implements PersistenceUnitPostProcessor, InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(MorganaPersistenceUnitPostProcessor.class);

    /**
     * the path of packages to search for persistent classes (e.g. org.springframework). Subpackages will be visited, too
     */
    private String[] packages;

    /**
     * the calculated list of additional persistent classes
     */
    private Set<Class<?>> persistentClasses = new HashSet<>();

    public MorganaPersistenceUnitPostProcessor() {
    }

    public MorganaPersistenceUnitPostProcessor(String... packages) {
        this.packages = packages;
    }

    /**
     * Looks for any persistent class in the class-path under the specified packages
     */
    @Override
    public void afterPropertiesSet() throws Exception {

        Collection<URL> urls = ClasspathHelper.forJavaClassPath();
        urls.addAll(ClasspathHelper.forClassLoader());
        Set<Class<?>> entityClasses = new Reflections(new ConfigurationBuilder()
                .setUrls(urls)).getTypesAnnotatedWith(Entity.class);

        for (String packageName : packages) {
            String pattern = buildScanPattern(packageName);
            for (Class<?> entityClass : entityClasses) {
                if (entityClass.getName().matches(pattern)) {
                    persistentClasses.add(entityClass);
                }
            }
        }
    }

    /**
     * Add all the persistent classes found to the PersistentUnit
     */
    @Override
    public void postProcessPersistenceUnitInfo(MutablePersistenceUnitInfo persistenceUnitInfo) {
        for (Class<?> c : persistentClasses) {
            LOGGER.debug("Found entity class: " + c.getName());
            persistenceUnitInfo.addManagedClassName(c.getName());
        }
    }

    public void setPackages(String... packages) {
        this.packages = packages;
    }

    /**
     * Builds the pattern used to scan packages.
     *
     * @param scanPackageName The name of the package to scan.
     * @return The pattern used to scan packages.
     */
    private static String buildScanPattern(final String scanPackageName) {

        return scanPackageName.replace(".", "\\.").replace("**", ".*") + "\\..*";
    }
}