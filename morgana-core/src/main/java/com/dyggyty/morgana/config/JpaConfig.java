package com.dyggyty.morgana.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author vitaly.rudenya
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories({"com.dyggyty.morgana.dao", "com.dyggyty.morgana.persistence"})
@PropertySource("classpath:META-INF/spring/persistence.properties")
public class JpaConfig {
    private static final String[] JPA_PROPERTIES = new String[]{
            "hibernate.dialect",
            "hibernate.format_sql",
            "hibernate.ejb.naming_strategy",
            "hibernate.show_sql",
            "hibernate.hbm2ddl.auto",
            "hibernate.cache.use_second_level_cache",
            "hibernate.cache.use_query_cache",
            "hibernate.archive.autodetection",
            "javax.persistence.sharedCache.mode"};

    private static final String ENTITY_MANAGER_PACKAGES_TO_SCAN = "entitymanager.packages.to.scan";

    private volatile LocalContainerEntityManagerFactoryBean entityManagerFactoryBean;
    private volatile Properties jpaProperties;

    @Resource
    private Environment environment;

    @Autowired
    @Qualifier("dataSource")
    private DataSource dataSource;

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean() throws Exception {
        if (entityManagerFactoryBean == null) {
            synchronized (this) {
                if (entityManagerFactoryBean == null) {

                    entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();

                    entityManagerFactoryBean.setPersistenceUnitName("punit");
                    entityManagerFactoryBean.setDataSource(dataSource);
                    entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
                    entityManagerFactoryBean.setJpaProperties(buildJpaProperties());
                    entityManagerFactoryBean.setJpaDialect(new HibernateJpaDialect());

                    String packageToScan = environment
                            .getRequiredProperty(ENTITY_MANAGER_PACKAGES_TO_SCAN);
                    MorganaPersistenceUnitPostProcessor persistenceUnitPostProcessor =
                            new MorganaPersistenceUnitPostProcessor(packageToScan);
                    persistenceUnitPostProcessor.afterPropertiesSet();
                    entityManagerFactoryBean.setPersistenceUnitPostProcessors(persistenceUnitPostProcessor);
                }
            }
        }

        return entityManagerFactoryBean;
    }

    @Bean(name = "txManager")
    public PlatformTransactionManager transactionManager() throws Exception {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactoryBean().getObject());
        txManager.setJpaProperties(buildJpaProperties());
        txManager.afterPropertiesSet();
        return txManager;
    }

    private Properties buildJpaProperties() {
        if (jpaProperties == null) {
            synchronized (this) {
                if (jpaProperties == null) {
                    jpaProperties = new Properties();
                    for (String propertyName : JPA_PROPERTIES) {
                        jpaProperties.put(propertyName, environment.getRequiredProperty(propertyName));
                    }
                }
            }
        }
        return jpaProperties;
    }
}
