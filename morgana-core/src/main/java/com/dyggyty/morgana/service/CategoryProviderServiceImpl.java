package com.dyggyty.morgana.service;

import com.dyggyty.morgana.bd.CategoryBd;
import com.dyggyty.morgana.dto.CategoryDto;
import com.dyggyty.morgana.framework.CategoryProvider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.ConfigurationException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class CategoryProviderServiceImpl implements CategoryProviderService {

    private final Log LOGGER = LogFactory.getLog(getClass());

    @Autowired
    private CategoryBd categoryBd;
    @Autowired(required = false)
    private Set<CategoryProvider> categoryProviders;

    @Override
    public List<CategoryDto> getCategories() {

        List<CategoryDto> categoryDtos = new ArrayList<>();
        Map<CategoryDto, CategoryDto> categoryDtoMap = new HashMap<>();

        if (categoryProviders != null) {
            categoryProviders.forEach(categoryProvider -> {
                try {
                    Set<CategoryDto> providerCategories = categoryProvider.getCategories();

                    //merging category children
                    providerCategories.forEach(categoryDto -> {
                        CategoryDto parentCategory = categoryDtoMap.get(categoryDto);
                        if (parentCategory == null) {
                            categoryDtoMap.put(categoryDto, categoryDto);
                            categoryDtos.add(categoryDto);
                        } else if (categoryDto.getChildren() != null) {
                            parentCategory.getChildren().addAll(categoryDto.getChildren());
                        }
                    });
                } catch (ConfigurationException e) {
                    LOGGER.error("Error retrieving categories", e);
                }
            });
        }

        Collections.sort(categoryDtos);
        categoryDtos.forEach(categoryDto -> Collections.sort(categoryDto.getChildren()));

        return categoryDtos;
    }
}
