package com.dyggyty.morgana.service;

import com.dyggyty.morgana.model.administration.Member;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * User: vitaly.rudenya
 * Date: 04.07.13
 * Time: 3:32
 */
public class UserDetailsContainer extends User {

    private Member member;

    public UserDetailsContainer(Member member, Collection<? extends GrantedAuthority> authorities) {
        super(member.getLogin(), member.getPassword(), authorities);
        this.member = member;
    }

    public Member getMember() {
        return member;
    }
}
