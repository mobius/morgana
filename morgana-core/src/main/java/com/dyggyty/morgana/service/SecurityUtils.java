package com.dyggyty.morgana.service;

import com.dyggyty.morgana.model.administration.Member;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author mikhail.hul
 */
public class SecurityUtils {
    public static Member getLoggedMember() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof UserDetailsContainer) { //authentication might be String for anonymous users
            UserDetailsContainer userDetails = (UserDetailsContainer) authentication.getPrincipal();
            return userDetails.getMember();
        }
        return null;
    }
}
