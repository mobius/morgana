package com.dyggyty.morgana.service;

import com.dyggyty.morgana.dto.AttachedEntityDescriptorDto;
import com.dyggyty.morgana.dto.EntityDescriptorDto;
import com.dyggyty.morgana.entity.EntityDescriptorProvider;
import com.dyggyty.morgana.entity.EntityExtender;
import com.dyggyty.morgana.persistence.EntityCrudRepository;
import com.dyggyty.morgana.persistence.EntityNameAware;
import com.dyggyty.morgana.utils.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class EntityDescriptorServiceImpl implements EntityDescriptorService {

    @Autowired
    private Set<EntityDescriptorProvider> entityDescriptorProviders;
    @Autowired(required = false)
    private Set<EntityExtender> entityExtenders;
    @Autowired
    private ApplicationContext appContext;
    private AutowireCapableBeanFactory autowireCapableBeanFactory;

    private final Map<String, EntityDescriptorDto> ENTITY_DESCRIPTORS = new HashMap<>();
    private final Log LOGGER = LogFactory.getLog(getClass());

    @PostConstruct
    public void init() {
        autowireCapableBeanFactory = appContext.getAutowireCapableBeanFactory();
    }

    @Override
    public EntityDescriptorDto getEntityDescriptor(Class entityClass) {

        EntityDescriptorDto entityDescriptorDto = null;
        if (entityDescriptorProviders != null) {
            for (EntityDescriptorProvider entityDescriptorProvider : entityDescriptorProviders) {
                entityDescriptorDto = entityDescriptorProvider.getEntityDescriptor(entityClass);
                if (entityDescriptorDto != null) {
                    setupAttachedEntities(entityDescriptorDto);
                    break;
                }
            }
        }

        return entityDescriptorDto;
    }

    @Override
    public EntityDescriptorDto getEntityDescriptor(String entityName) {

        return CollectionUtils.getOrCreateSynchronously(ENTITY_DESCRIPTORS, entityName, () -> {
            EntityDescriptorDto entityDescriptorDto = null;
            if (entityDescriptorProviders != null) {
                for (EntityDescriptorProvider entityDescriptorProvider : entityDescriptorProviders) {
                    entityDescriptorDto = entityDescriptorProvider.getEntityDescriptor(entityName);
                    if (entityDescriptorDto != null) {
                        setupAttachedEntities(entityDescriptorDto);
                        break;
                    }
                }
            }

            return entityDescriptorDto;
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    public <KEY extends Serializable> EntityCrudRepository<?, KEY> getCrudRepository(String entityName) {

        EntityDescriptorDto entityDescriptorDto = getEntityDescriptor(entityName);
        Class<? extends EntityCrudRepository> repositoryClass = entityDescriptorDto.getCrudRepository();

        return getCrudRepository(repositoryClass, entityName);
    }

    @Override
    public <KEY extends Serializable, T extends EntityCrudRepository<?, KEY>> T getCrudRepository(
            Class<T> repositoryClass, String entityName) {

        T entityCrudRepository = null;

        try {
            entityCrudRepository = repositoryClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            LOGGER.error("Can't create repository class instance for " + repositoryClass.getName());
        }

        if (entityCrudRepository != null) {
            if (entityCrudRepository instanceof EntityNameAware) {

                if (StringUtils.isEmpty(entityName)) {
                    throw new IllegalArgumentException("No entity name was specified");
                }

                ((EntityNameAware) entityCrudRepository).setEntityName(entityName);
            }
            autowireCapableBeanFactory.autowireBean(entityCrudRepository);
        }

        return entityCrudRepository;
    }

    private void setupAttachedEntities(EntityDescriptorDto entityDescriptorDto) {

        //Setup attached entities
        List<AttachedEntityDescriptorDto> attachedEntities = new ArrayList<>();
        final Class mainEntityClass = entityDescriptorDto.getEntityClass();
        if (entityExtenders != null) {
            for (EntityExtender entityExtender : entityExtenders) {
                AttachedEntityDescriptorDto classExtension =
                        entityExtender.getAttacheEntityDescriptor(mainEntityClass);
                if (classExtension != null) {
                    attachedEntities.add(classExtension);
                }
            }

            entityDescriptorDto.setAttachedEntityDescriptorDtos(attachedEntities);
        }
    }
}
