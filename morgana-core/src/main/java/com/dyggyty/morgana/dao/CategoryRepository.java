package com.dyggyty.morgana.dao;

import com.dyggyty.morgana.model.category.Category;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {

    public static final String GET_TOP_LEVEL = "SELECT cat FROM Category cat " +
            "WHERE cat.parent IS NULL";

    @Query(GET_TOP_LEVEL)
    public Iterable<Category> findTopLevel();
}
