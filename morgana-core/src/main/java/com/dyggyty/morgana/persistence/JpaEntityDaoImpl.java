package com.dyggyty.morgana.persistence;

import com.dyggyty.morgana.utils.ReflectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.type.Type;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.query.QueryUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.springframework.data.jpa.repository.query.QueryUtils.COUNT_QUERY_STRING;
import static org.springframework.data.jpa.repository.query.QueryUtils.DELETE_ALL_QUERY_STRING;
import static org.springframework.data.jpa.repository.query.QueryUtils.applyAndBind;
import static org.springframework.data.jpa.repository.query.QueryUtils.getQueryString;
import static org.springframework.data.jpa.repository.query.QueryUtils.toOrders;

@Repository
@Transactional(
        readOnly = true
)
public class JpaEntityDaoImpl implements JpaEntityDao {

    private final Log LOGGER = LogFactory.getLog(getClass());

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void delete(Serializable id, String entityName) {

        Assert.notNull(id, "The given id must not be null!");

        Object entity = findOne(id, entityName);

        if (entity == null) {
            throw new EmptyResultDataAccessException(String.format("No %s entity with id %s exists!",
                    entityName, id), 1);
        }

        delete(entity);
    }

    @Override
    @Transactional
    public void delete(String id, String entityName) {
        Assert.notNull(id, "The given id must not be null!");

        Serializable entityId = convertToIdentifier(entityName, id);
        delete(entityId, entityName);
    }

    @Override
    @Transactional
    public void delete(Object entity) {

        Assert.notNull(entity, "The entity must not be null!");
        entityManager.remove(entityManager.contains(entity) ? entity : entityManager.merge(entity));
    }

    @Override
    @Transactional
    public void delete(Iterable entities) {

        Assert.notNull(entities, "The given Iterable of entities not be null!");

        for (Object entity : entities) {
            delete(entity);
        }
    }

    @Override
    @Transactional
    public void deleteInBatch(Iterable entities, String entityName) {

        Assert.notNull(entities, "The given Iterable of entities not be null!");

        if (!entities.iterator().hasNext()) {
            return;
        }

        applyAndBind(getQueryString(DELETE_ALL_QUERY_STRING, entityName), entities, entityManager)
                .executeUpdate();
    }

    @Override
    @Transactional
    public void deleteAll(String entityName) {

        findAll(entityName).forEach(this::delete);
    }

    @Override
    public Object findOne(String id, String entityName) {
        Assert.notNull(id, "The given id must not be null!");
        Serializable entityId = convertToIdentifier(entityName, id);
        return findOne(entityId, entityName);
    }

    @Override
    public Object findOne(Serializable id, String entityName) {

        Assert.notNull(id, "The given id must not be null!");
        Class domainType = getDomainClass(entityName);
        return entityManager.find(domainType, id);
    }

    @Override
    public Object getOne(Serializable id, String entityName) {

        Assert.notNull(id, "The given id must not be null!");
        return entityManager.getReference(getDomainClass(entityName), id);
    }

    @Override
    public List<Object> findAll(String entityName) {
        return getQuery(null, (Sort) null, entityName).getResultList();
    }

    @Override
    public List<Object> findAll(Sort sort, String entityName) {
        return getQuery(null, sort, entityName).getResultList();
    }

    @Override
    public Page<Object> findAll(Pageable pageable, String entityName) {

        if (null == pageable) {
            return new PageImpl<>(findAll(entityName));
        }

        return findAll(null, pageable, entityName);
    }

    @Override
    public Object findOne(Specification spec, String entityName) {

        try {
            return getQuery(spec, (Sort) null, entityName).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List findAll(Specification spec, String entityName) {
        return getQuery(spec, (Sort) null, entityName).getResultList();
    }

    @Override
    public Page<Object> findAll(Specification spec, Pageable pageable, String entityName) {

        TypedQuery<Object> query = getQuery(spec, pageable, entityName);
        return pageable == null ? new PageImpl<>(query.getResultList()) : readPage(query, pageable, spec, entityName);
    }

    @Override
    public List findAll(Specification spec, Sort sort, String entityName) {

        return getQuery(spec, sort, entityName).getResultList();
    }

    @Override
    public long count(String entityName) {
        return entityManager.createQuery(getCountQueryString(entityName), Long.class).getSingleResult();
    }

    @Override
    public long count(Specification spec, String entityName) {

        return getCountQuery(spec, entityName).getSingleResult();
    }

    @Override
    @Transactional
    public Object save(Object entity) {
        if (isNew(entity)) {
            entityManager.persist(entity);
            return entity;
        } else {
            return entityManager.merge(entity);
        }
    }

    @Override
    @Transactional
    public Object save(Map<String, String> entity, String entityName) {

        ClassMetadata classMetadata = getClassMetadata(entityName);

        try {
            Object entityInstance = ReflectionUtils.createObject(classMetadata, entity);
            return save(entityInstance);
        } catch (ReflectiveOperationException e) {
            LOGGER.error(e);
            throw new IllegalAccessError("Can't create instance of the " +
                    classMetadata.getMappedClass().getName() + "\n" + e.getLocalizedMessage());
        }
    }

    @Override
    @Transactional
    public Object saveAndFlush(Object entity) {

        Object result = save(entity);
        flush();

        return result;
    }

    @Override
    @Transactional
    public List save(Iterable<Object> entities) {

        final List<Object> result = new ArrayList<>();

        if (entities == null) {
            return result;
        }

        entities.forEach(entity -> result.add(save(entity)));

        return result;
    }

    @Override
    @Transactional
    public void flush() {
        entityManager.flush();
    }

    /**
     * Retrieve entity type by name;
     *
     * @param entityName Name of th entity.
     * @return Entity type.
     */
    public <T> EntityType<T> getEntityType(String entityName) {
        Set<EntityType<?>> entityTypes = entityManager.getMetamodel().getEntities();

        for (EntityType<?> entityType : entityTypes) {
            if (entityType.getName().equals(entityName)) {
                //noinspection unchecked
                return (EntityType<T>)entityType;
            }
        }
        return null;
    }

    @Override
    public <T> EntityType<T> getEntityType(Class<T> entityClass) {
        Set<EntityType<?>> entityTypes = entityManager.getMetamodel().getEntities();

        for (EntityType<?> entityType : entityTypes) {
            if (entityType.getBindableJavaType().equals(entityClass)) {
                //noinspection unchecked
                return (EntityType<T>)entityType;
            }
        }
        return null;
    }


    /**
     * Reads the given {@link TypedQuery} into a {@link Page} applying the given {@link Pageable} and
     * {@link Specification}.
     *
     * @param query      must not be {@literal null}.
     * @param spec       can be {@literal null}.
     * @param pageable   can be {@literal null}.
     * @param entityName Name of the entity. Must not be {@literal null}.
     * @return Requested page.
     */
    protected Page readPage(TypedQuery query, Pageable pageable, Specification spec, String entityName) {

        query.setFirstResult(pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());

        Long total = QueryUtils.executeCountQuery(getCountQuery(spec, entityName));
        List<Object> content = total > pageable.getOffset() ? query.getResultList() : Collections.emptyList();

        return new PageImpl<>(content, pageable, total);
    }

    /**
     * Creates a new {@link TypedQuery} from the given {@link Specification}.
     *
     * @param spec       can be {@literal null}.
     * @param pageable   can be {@literal null}.
     * @param entityName Name of the entity. Must not be {@literal null}.
     * @return Query
     */
    protected TypedQuery<Object> getQuery(Specification spec, Pageable pageable, String entityName) {

        Sort sort = pageable == null ? null : pageable.getSort();
        return getQuery(spec, sort, entityName);
    }

    /**
     * Creates a {@link TypedQuery} for the given {@link Specification} and {@link Sort}.
     *
     * @param spec       can be {@literal null}.
     * @param sort       can be {@literal null}.
     * @param entityName Name of the entity. Must not be {@literal null}.
     * @return Query.
     */
    protected TypedQuery<Object> getQuery(Specification spec, Sort sort, String entityName) {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery query = builder.createQuery(getDomainClass(entityName));

        Root root = applySpecificationToCriteria(spec, query, entityName);
        query.select(root);

        if (sort != null) {
            query.orderBy(toOrders(sort, root, builder));
        }

        return entityManager.createQuery(query);
    }

    /**
     * Creates a new count query for the given {@link Specification}.
     *
     * @param spec       can be {@literal null}.
     * @param entityName Name of the entity. Must not be {@literal null}.
     * @return Query.
     */
    protected TypedQuery<Long> getCountQuery(Specification spec, String entityName) {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);

        Root root = applySpecificationToCriteria(spec, query, entityName);

        if (query.isDistinct()) {
            query.select(builder.countDistinct(root));
        } else {
            query.select(builder.count(root));
        }

        return entityManager.createQuery(query);
    }

    /**
     * Applies the given {@link Specification} to the given {@link CriteriaQuery}.
     *
     * @param spec       can be {@literal null}.
     * @param query      must not be {@literal null}.
     * @param entityName Name of the entity. Must not be {@literal null}.
     * @return
     */
    private <S> Root applySpecificationToCriteria(Specification spec, CriteriaQuery<S> query, String entityName) {

        Assert.notNull(query);
        Root root = query.from(getDomainClass(entityName));

        if (spec == null) {
            return root;
        }

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        Predicate predicate = spec.toPredicate(root, query, builder);

        if (predicate != null) {
            query.where(predicate);
        }

        return root;
    }

    private boolean isNew(Object entity) {

        Object id = entityManager.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(entity);
        Class idType = entityManager.getMetamodel().entity(entity.getClass()).getIdType().getJavaType();

        if (!idType.isPrimitive()) {
            return id == null;
        }

        if (id instanceof Number) {
            return ((Number) id).longValue() == 0L;
        }

        throw new IllegalArgumentException(String.format("Unsupported primitive id type %s!", idType));
    }

    private Class getDomainClass(String entityName) {

        if (StringUtils.isEmpty(entityName)) {
            throw new IllegalArgumentException("Entity name is empty");
        }

        Set<EntityType<?>> entityTypes = entityManager.getMetamodel().getEntities();

        for (EntityType<?> entityType : entityTypes) {
            if (entityName.equals(entityType.getName())) {
                return entityType.getBindableJavaType();
            }
        }

        throw new IllegalArgumentException(String.format("Unsupported entity %s!", entityName));
    }

    private Serializable convertToIdentifier(String entityName, String strId) {

        ClassMetadata classMetadata = getClassMetadata(entityName);
        Type idType = classMetadata.getIdentifierType();

        return (Serializable) ReflectionUtils.getFieldValue(idType, strId);
    }

    private ClassMetadata getClassMetadata(String entityName) {
        HibernateEntityManagerFactory entityManagerFactory =
                (HibernateEntityManagerFactory) entityManager.getEntityManagerFactory();
        EntityType entityType = getEntityType(entityName);
        return entityManagerFactory.getSessionFactory().getClassMetadata(entityType.getBindableJavaType());
    }

    private static String getCountQueryString(String entityName) {

        return String.format(COUNT_QUERY_STRING, "*", entityName);
    }
}
