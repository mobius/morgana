package com.dyggyty.morgana.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Provides JPA CRUD operations for the common classes.
 */
@Transactional
public class JpaEntityCrudRepositoryImpl implements EntityCrudRepository<Object, Serializable>, EntityNameAware {

    @Autowired
    private JpaEntityDao jpaEntityDao;

    private String entityName;

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    @Override
    public void delete(Serializable id) {
        jpaEntityDao.delete(id, entityName);
    }

    @Override
    public void deleteByString(String id) {
        jpaEntityDao.delete(id, entityName);
    }

    @Override
    public void delete(Object entity) {
        jpaEntityDao.delete(entity);
    }

    @Override
    public void delete(Iterable entities) {
        jpaEntityDao.delete(entities);
    }

    @Override
    public void deleteInBatch(Iterable entities) {
        jpaEntityDao.deleteInBatch(entities, entityName);
    }

    @Override
    public void deleteAll() {
        jpaEntityDao.deleteAll(entityName);
    }

    @Override
    public Object findOne(Serializable id) {
        return jpaEntityDao.findOne(id, entityName);
    }

    @Override
    public Object findOne(String id) {
        return jpaEntityDao.findOne(id, entityName);
    }

    @Override
    public List<Object> findAll() {
        return jpaEntityDao.findAll(entityName);
    }

    @Override
    public List<Object> findAll(Sort sort) {
        return jpaEntityDao.findAll(sort, entityName);
    }

    @Override
    public Page<Object> findAll(Pageable pageable) {
        return jpaEntityDao.findAll(pageable, entityName);
    }

    @Override
    public long count() {
        return jpaEntityDao.count(entityName);
    }

    @Override
    public Object save(Map<String, String> entity) {
        return jpaEntityDao.save(entity, entityName);
    }

    @Override
    public Object save(Object entity) {
        return jpaEntityDao.save(entity);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List save(Iterable entities) {
        return jpaEntityDao.save(entities);
    }
}
