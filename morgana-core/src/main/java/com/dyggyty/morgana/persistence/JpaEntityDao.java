package com.dyggyty.morgana.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.metamodel.EntityType;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface JpaEntityDao {

    /**
     * @see org.springframework.data.repository.CrudRepository#delete(java.io.Serializable)
     */
    void delete(Serializable id, String entityName);

    /**
     * @param id String representation of the identifier.
     * @see org.springframework.data.repository.CrudRepository#delete(java.io.Serializable)
     */
    void delete(String id, String entityName);

    /**
     * @see org.springframework.data.repository.CrudRepository#delete(java.lang.Object)
     */
    void delete(Object entity);

    /**
     * @see org.springframework.data.repository.CrudRepository#delete(java.lang.Iterable)
     */
    void delete(Iterable entities);

    /**
     * @see org.springframework.data.jpa.repository.JpaRepository#deleteInBatch(java.lang.Iterable)
     */
    void deleteInBatch(Iterable entities, String entityName);

    /**
     * @see CrudRepository#deleteAll() eleteAll()
     */
    void deleteAll(String entityName);

    /**
     * @param id String representation of the identifier.
     * @see org.springframework.data.repository.CrudRepository#findOne(java.io.Serializable)
     */
    Object findOne(String id, String entityName);

    /**
     * @see org.springframework.data.repository.CrudRepository#findOne(java.io.Serializable)
     */
    Object findOne(Serializable id, String entityName);

    /**
     * @see org.springframework.data.jpa.repository.JpaRepository#getOne(java.io.Serializable)
     */
    Object getOne(Serializable id, String entityName);

    /**
     * @see org.springframework.data.jpa.repository.JpaRepository#findAll()
     */
    List<Object> findAll(String entityName);

    /**
     * @see org.springframework.data.jpa.repository.JpaRepository#findAll(org.springframework.data.domain.Sort)
     */
    List<Object> findAll(Sort sort, String entityName);

    /**
     * @see org.springframework.data.repository.PagingAndSortingRepository#findAll(org.springframework.data.domain.Pageable)
     */
    Page<Object> findAll(Pageable pageable, String entityName);

    /**
     * @see org.springframework.data.jpa.repository.JpaSpecificationExecutor#findOne(org.springframework.data.jpa.domain.Specification)
     */
    Object findOne(Specification spec, String entityName);

    /**
     * @see org.springframework.data.jpa.repository.JpaSpecificationExecutor#findAll(org.springframework.data.jpa.domain.Specification)
     */
    List findAll(Specification spec, String entityName);

    /**
     * @see org.springframework.data.jpa.repository.JpaSpecificationExecutor#findAll(org.springframework.data.jpa.domain.Specification, org.springframework.data.domain.Pageable)
     */
    Page<Object> findAll(Specification spec, Pageable pageable, String entityName);

    /**
     * @see org.springframework.data.jpa.repository.JpaSpecificationExecutor#findAll(org.springframework.data.jpa.domain.Specification, org.springframework.data.domain.Sort)
     */
    List findAll(Specification spec, Sort sort, String entityName);

    /**
     * @see org.springframework.data.repository.CrudRepository#count()
     */
    long count(String entityName);

    /**
     * @see org.springframework.data.jpa.repository.JpaSpecificationExecutor#count(org.springframework.data.jpa.domain.Specification)
     */
    long count(Specification spec, String entityName);

    /**
     * @see org.springframework.data.repository.CrudRepository#save(java.lang.Object)
     */
    Object save(Object entity);

    /**
     * Save the entity from mapped values.
     *
     * @param entity     Map of the field and values.
     * @param entityName Name of the entity.
     * @return Saved entity
     * @see org.springframework.data.repository.CrudRepository#save(java.lang.Object)
     */
    Object save(Map<String, String> entity, String entityName);

    /**
     * @see org.springframework.data.jpa.repository.JpaRepository#saveAndFlush(java.lang.Object)
     */
    Object saveAndFlush(Object entity);

    /**
     * @see org.springframework.data.jpa.repository.JpaRepository#save(java.lang.Iterable)
     */
    List save(Iterable<Object> entities);

    /**
     * @see org.springframework.data.jpa.repository.JpaRepository#flush()
     */
    void flush();

    /**
     * Retrieve entity type by name;
     *
     * @param entityName Name of the entity.
     * @return Entity type.
     */
    <T> EntityType<T> getEntityType(String entityName);

    /**
     * Retrieve entity type by name;
     *
     * @param entityClass Class of the entity.
     * @return Entity type.
     */
    <T> EntityType<T> getEntityType(Class<T> entityClass);
}
