package com.dyggyty.morgana.framework;

import com.dyggyty.morgana.dto.EntityActionDto;
import com.dyggyty.morgana.framework.function.PluginFunction;
import com.dyggyty.morgana.utils.CollectionUtils;
import org.apache.commons.collections4.keyvalue.DefaultMapEntry;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class PluginFunctionFactoryImpl implements PluginFunctionFactory, Serializable {

    private final Log LOGGER = LogFactory.getLog(getClass());

    private static final Map<String, Map<String, PluginFunction>> PLUGIN_FUNCTIONS = new HashMap<>();
    private static final Map<Class<? extends PluginFunction>, String> FUNCTION_PLUGINS = new HashMap<>();

    @Autowired
    private ApplicationContext appContext;
    @Autowired(required = false)
    private Set<Plugin> plugins;

    public <T extends PluginFunction> Map.Entry<String, T> getPluginFunction(Class<T> pluginFunctionClass) {

        T function = (T) appContext.getBean(pluginFunctionClass.getCanonicalName());
        if (function == null) {
            return null;
        }

        String pluginName = FUNCTION_PLUGINS.get(pluginFunctionClass);

        if (pluginName == null) {
            throw new IllegalStateException("Plugin function '" + function.getFunctionName() + "' without plugin.");
        }

        return new DefaultMapEntry<>(pluginName, function);
    }

    @Override
    public List<EntityActionDto> getDefaultActions(Class entityType) {

        List<EntityActionDto> entityActionDtos = new ArrayList<>();

        PLUGIN_FUNCTIONS.entrySet().forEach(pluginsFunctionsEntry -> {

            String pluginName = pluginsFunctionsEntry.getKey();
            pluginsFunctionsEntry.getValue().entrySet().forEach(pluginFunctionsEntry -> {

                PluginFunction pluginFunction = pluginFunctionsEntry.getValue();

                boolean defaultOnList = pluginFunction.isDefaultOnEntityList(entityType);
                boolean defaultOnForm = pluginFunction.isDefaultOnEntityForm(entityType);

                if (defaultOnForm || defaultOnList) {

                    Class<?> pluginFunctionClass = AopUtils.getTargetClass(pluginFunction);
                    EntityDefaultFunction defaultFunction =
                            pluginFunctionClass.getAnnotation(EntityDefaultFunction.class);

                    if (defaultFunction != null) {
                        EntityActionDto entityActionDto =
                                new EntityActionDto(pluginName, pluginFunctionsEntry.getKey());

                        String description = StringUtils.isEmpty(defaultFunction.description()) ?
                                null : defaultFunction.description();
                        entityActionDto.setDescription(description);

                        String image = StringUtils.isEmpty(defaultFunction.image()) ? null : defaultFunction.image();
                        entityActionDto.setImage(image);

                        entityActionDto.setTitle(defaultFunction.title());

                        entityActionDto.setVisibleOnForm(defaultOnForm);
                        entityActionDto.setVisibleOnList(defaultOnList);

                        entityActionDtos.add(entityActionDto);
                    }
                }
            });
        });

        return entityActionDtos;
    }

    @Override
    public Plugin getPlugin(String plugin) {
        return appContext.getBean(plugin, Plugin.class);
    }

    @Override
    public PluginFunction getPluginFunction(Plugin plugin, String function) {

        if (plugin == null) {
            LOGGER.warn("Can't find requested plugin");
            return null;
        }

        String pluginName = plugin.getName();
        Map<String, PluginFunction> pluginFunctions =
                CollectionUtils.getOrCreateSynchronously(PLUGIN_FUNCTIONS, pluginName,
                        () -> registerPluginFunctions(plugin));

        return pluginFunctions == null ? null : pluginFunctions.get(function);
    }

    private Map<String, PluginFunction> registerPluginFunctions(Plugin requestedPlugin) {
        List<Class<? extends PluginFunction>> pluginFunctionClasses = requestedPlugin.getPluginFunctions();

        String pluginName = requestedPlugin.getName();
        if (pluginFunctionClasses != null) {
            Map<String, PluginFunction> initializedPluginFunctions = new HashMap<>();
            ConfigurableListableBeanFactory autowireCapableBeanFactory =
                    (ConfigurableListableBeanFactory) appContext.getAutowireCapableBeanFactory();

            pluginFunctionClasses.forEach(pluginFunctionClass -> {
                PluginFunction pluginFunction = null;
                try {
                    pluginFunction = appContext.getBean(pluginFunctionClass);
                } catch (NoSuchBeanDefinitionException noSuchBean) {
                    try {
                        pluginFunction = pluginFunctionClass.newInstance();
                        autowireCapableBeanFactory.autowireBean(pluginFunction);
                        pluginFunction = (PluginFunction) autowireCapableBeanFactory.
                                initializeBean(pluginFunction, pluginFunctionClass.getCanonicalName());
                        autowireCapableBeanFactory.
                                registerSingleton(pluginFunctionClass.getCanonicalName(), pluginFunction);
                        pluginFunction.setPlugin(requestedPlugin);
                    } catch (InstantiationException | IllegalAccessException e) {
                        LOGGER.error("Can't instantiate plugin function " + pluginFunctionClass.getName(), e);
                    }
                }

                if (pluginFunction != null) {
                    FUNCTION_PLUGINS.put(pluginFunctionClass, pluginName);
                    initializedPluginFunctions.put(pluginFunction.getFunctionName(), pluginFunction);
                }
            });

            PLUGIN_FUNCTIONS.put(pluginName, initializedPluginFunctions);

            return initializedPluginFunctions;
        }

        return Collections.emptyMap();
    }

    @PostConstruct
    public void init() {
        if (plugins != null) {
            plugins.forEach(this::registerPluginFunctions);
        }
    }
}
