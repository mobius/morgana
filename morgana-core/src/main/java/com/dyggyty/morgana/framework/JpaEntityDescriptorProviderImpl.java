package com.dyggyty.morgana.framework;

import com.dyggyty.morgana.configuration.EntityAction;
import com.dyggyty.morgana.configuration.EntityDescriptor;
import com.dyggyty.morgana.configuration.FieldDescriptor;
import com.dyggyty.morgana.configuration.MenuItem;
import com.dyggyty.morgana.dto.AbstractDocumentFieldDto;
import com.dyggyty.morgana.dto.CategoryDto;
import com.dyggyty.morgana.dto.CollectionFieldDto;
import com.dyggyty.morgana.dto.DocumentFieldDto;
import com.dyggyty.morgana.dto.EntityActionDto;
import com.dyggyty.morgana.dto.EntityDescriptorDto;
import com.dyggyty.morgana.entity.EntityDescriptorProvider;
import com.dyggyty.morgana.entity.FieldTemplate;
import com.dyggyty.morgana.framework.function.PluginFunction;
import com.dyggyty.morgana.persistence.DefaultRepository;
import com.dyggyty.morgana.persistence.EntityCrudRepository;
import com.dyggyty.morgana.persistence.JpaEntityCrudRepositoryImpl;
import com.dyggyty.morgana.persistence.JpaEntityDao;
import com.dyggyty.morgana.utils.EntityUtils;
import com.dyggyty.morgana.utils.ReflectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.OneToMany;
import javax.persistence.PersistenceContext;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.Attribute.PersistentAttributeType;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.PluralAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.Type;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Member;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class JpaEntityDescriptorProviderImpl implements EntityDescriptorProvider {

    private final Log LOGGER = LogFactory.getLog(getClass());

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private JpaEntityDao jpaEntityDao;
    @Autowired
    private PluginFunctionFactory pluginFunctionFactory;

    public Set<EntityDescriptorDto> getEntityDescriptors() {

        final Set<EntityDescriptorDto> entityDescriptorDtos = new HashSet<>();
        Set<EntityType<?>> entityTypes = entityManager.getMetamodel().getEntities();

        entityTypes.forEach(entityType -> entityDescriptorDtos.add(getEntityDescriptor(entityType)));

        return entityDescriptorDtos;
    }

    @Override
    public EntityDescriptorDto getEntityDescriptor(String entityName) {

        EntityType<?> entityType = jpaEntityDao.getEntityType(entityName);
        if (entityType != null) {
            return getEntityDescriptor(entityType);
        }

        return null;
    }

    @Override
    public <T> EntityDescriptorDto<T> getEntityDescriptor(Class<T> entityClass) {
        EntityType<T> entityType = jpaEntityDao.getEntityType(entityClass);
        if (entityType != null) {
            return getEntityDescriptor(entityType);
        }

        return null;
    }

    /**
     * Create entity descriptor from the JPA entity type
     *
     * @param entityType Type of the JPA entity.
     * @return Entity descriptor.
     */
    private <T> EntityDescriptorDto<T> getEntityDescriptor(EntityType<T> entityType) {

        Class<?> entityClass = entityType.getBindableJavaType();
        MenuItem entityMenuItem = entityClass.getDeclaredAnnotation(MenuItem.class);

        EntityDescriptorDto<T> entityDescriptorDto = new EntityDescriptorDto<>();

        if (entityMenuItem != null) {
            entityDescriptorDto.setCategoryName(entityMenuItem.value());
            entityDescriptorDto.setOrder(entityMenuItem.order());
        }

        List<EntityActionDto> actionDtos = new ArrayList<>();

        List<EntityActionDto> entityActionDtos = pluginFunctionFactory.getDefaultActions(entityClass);
        actionDtos.addAll(entityActionDtos);

        Class<? extends EntityCrudRepository> entityCrudRepository = JpaEntityCrudRepositoryImpl.class;
        EntityDescriptor entityDescriptor = entityClass.getDeclaredAnnotation(EntityDescriptor.class);
        if (entityDescriptor != null) {
            entityDescriptorDto.setEntityTitle(entityDescriptor.value());
            entityDescriptorDto.setListFormTitle(entityDescriptor.value());

            Class<? extends EntityCrudRepository> repositoryClass = entityDescriptor.repositoryClass();
            if (!DefaultRepository.class.equals(repositoryClass)) {
                entityCrudRepository = repositoryClass;
            }

            //Retrieve entity actions
            EntityAction[] entityActions = entityDescriptor.actions();
            for (EntityAction entityAction : entityActions) {
                Map.Entry<String, ? extends PluginFunction> pluginFunctionEntry =
                        pluginFunctionFactory.getPluginFunction(entityAction.function());
                PluginFunction pluginFunction = pluginFunctionEntry.getValue();
                EntityActionDto entityActionDto =
                        new EntityActionDto(pluginFunctionEntry.getKey(), pluginFunction.getFunctionName());
                entityActionDto.setDescription(StringUtils.isEmpty(entityAction.description()) ?
                        null : entityAction.description());
                entityActionDto.setImage(StringUtils.isEmpty(entityAction.image()) ? null : entityAction.image());

                entityActionDto.setTitle(entityAction.title());
                entityActionDto.setVisibleOnForm(entityAction.visibleOnForm());
                entityActionDto.setVisibleOnList(entityAction.visibleOnList());
                entityActionDto.setOrder(entityAction.order());

                actionDtos.add(entityActionDto);
            }
        }

        if (StringUtils.isEmpty(entityDescriptorDto.getEntityTitle())) {
            entityDescriptorDto.setEntityTitle(entityClass.getName());
        }
        if (StringUtils.isEmpty(entityDescriptorDto.getListFormTitle())) {
            entityDescriptorDto.setListFormTitle(entityClass.getName());
        }

        Collections.sort(actionDtos);
        entityDescriptorDto.setEntityActionDtos(actionDtos);

        entityDescriptorDto.setCrudRepository(entityCrudRepository);
        entityDescriptorDto.setEntityName(entityType.getName());
        entityDescriptorDto.setEntityClass(entityType.getBindableJavaType());

        CategoryDto parentalCategory = ReflectionUtils.getParentCategoryItem(entityClass.getPackage());
        entityDescriptorDto.setParentalCategory(parentalCategory);

        List<DocumentFieldDto> documentFields = new LinkedList<>();
        List<CollectionFieldDto> documentFieldTabs = new LinkedList<>();
        entityType.getAttributes()
                .forEach(attribute -> {
                    AbstractDocumentFieldDto abstractDocumentFieldDto = createDocumentFieldDto(attribute);
                    if (DocumentFieldDto.class.isInstance(abstractDocumentFieldDto)) {
                        documentFields.add((DocumentFieldDto) abstractDocumentFieldDto);
                    } else {
                        documentFieldTabs.add((CollectionFieldDto) abstractDocumentFieldDto);
                    }
                });

        Collections.sort(documentFields);
        entityDescriptorDto.setDocumentFields(documentFields);
        entityDescriptorDto.setDocumentFieldTabs(documentFieldTabs);

        return entityDescriptorDto;
    }

    private AbstractDocumentFieldDto createDocumentFieldDto(Attribute attribute) {

        FieldTemplate template = null;
        FieldDescriptor fieldDescriptor = null;

        Member member = attribute.getJavaMember();
        if (member instanceof AnnotatedElement) {
            AnnotatedElement annotatedElement = (AnnotatedElement) member;

            fieldDescriptor = annotatedElement.getAnnotation(FieldDescriptor.class);
            if (fieldDescriptor != null) {
                template = fieldDescriptor.template();
            }
        }

        //Trying to autodetect field type
        Class javaClass = attribute.getJavaType();
        if (template == null || FieldTemplate.AUTO.equals(template)) {
            template = EntityUtils.getFieldTemplate(javaClass, this);
        }

        AbstractDocumentFieldDto documentFieldDto;
        PersistentAttributeType persistentAttributeType = attribute.getPersistentAttributeType();
        String bindableEntityType = getBindableJavaType(attribute, jpaEntityDao);
        if (PersistentAttributeType.MANY_TO_MANY.equals(persistentAttributeType)) {
            documentFieldDto = new CollectionFieldDto(member, bindableEntityType);
        } else if (PersistentAttributeType.ONE_TO_MANY.equals(persistentAttributeType)) {
            Member mappadByMember = null;
            if (member instanceof AnnotatedElement) {
                AnnotatedElement annotatedElement = (AnnotatedElement) member;

                OneToMany oneToMany = annotatedElement.getAnnotation(OneToMany.class);
                if (oneToMany != null) {
                    String mappedBy = oneToMany.mappedBy();
                    if (StringUtils.isEmpty(mappedBy)) {
                        LOGGER.warn("Empty mappedby for the " + member.getName() + " in the @OneToMany.\n" +
                                "Possible problems with persistence operations.");
                    } else {
                        mappadByMember = getFieldMember(bindableEntityType, mappedBy);
                    }
                }
            }

            if (mappadByMember == null && LOGGER.isWarnEnabled()) {
                LOGGER.warn("Can't find referenced entity member.");
            }

            CollectionFieldDto collectionFieldDto = new CollectionFieldDto(member, bindableEntityType);
            collectionFieldDto.setReferencedField(mappadByMember);
            documentFieldDto = collectionFieldDto;
        } else {
            boolean hideOnTable = fieldDescriptor != null ? fieldDescriptor.hideOnTable() : FieldTemplate.PASSWORD.equals(template);
            boolean isVersion = false;
            boolean isId = false;

            if (attribute instanceof SingularAttribute) {
                SingularAttribute singularAttribute = (SingularAttribute) attribute;
                isVersion = singularAttribute.isVersion();
                isId = singularAttribute.isId();
            }

            int length = 0;
            if (member instanceof AnnotatedElement) {
                AnnotatedElement annotatedElement = (AnnotatedElement) member;

                Column column = annotatedElement.getAnnotation(Column.class);
                if (column != null) {
                    length = column.length();
                }
            }

            documentFieldDto = new DocumentFieldDto(member, bindableEntityType, template, length, hideOnTable, isId, isVersion);
        }

        documentFieldDto.setName(attribute.getName());

        if (fieldDescriptor != null) {
            String title = fieldDescriptor.value();
            documentFieldDto.setTitle(StringUtils.isEmpty(title) ? attribute.getName() : title);
            documentFieldDto.setOrder(fieldDescriptor.order());
            documentFieldDto.setHideOnForm(fieldDescriptor.hideOnForm());
        }


        return documentFieldDto;
    }

    private static String getBindableJavaType(Attribute attribute, JpaEntityDao jpaEntityDao) {

        EntityType entityType = null;

        if (attribute instanceof PluralAttribute) {
            Class entityClass = ((PluralAttribute) attribute).getBindableJavaType();
            entityType = entityClass == null ? null : jpaEntityDao.getEntityType(entityClass);
        } else if (attribute instanceof SingularAttribute) {
            Type attributeType = ((SingularAttribute) attribute).getType();
            if (attributeType != null && attributeType instanceof EntityType) {
                entityType = (EntityType) attributeType;
            }
        }

        return entityType != null ? entityType.getName() : null;
    }

    private Member getFieldMember(String entityTypeName, String fieldName) {
        EntityType entityType = jpaEntityDao.getEntityType(entityTypeName);
        Attribute entityAttribute = entityType.getAttribute(fieldName);
        return entityAttribute.getJavaMember();
    }
}