package com.dyggyty.morgana.bd;

import com.dyggyty.morgana.dao.CategoryRepository;
import com.dyggyty.morgana.model.category.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class CategoryBdImpl implements CategoryBd {

    //@Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> findAll() {
        return toList(categoryRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Category> findTopLevel() {
        return toList(categoryRepository.findTopLevel(), true);
    }

    private static List<Category> toList(Iterable<Category> categories) {
        return toList(categories, false);
    }

    private static List<Category> toList(Iterable<Category> categories, boolean initChildren) {
        List<Category> categoryList = new ArrayList<>();
        categories.forEach(category -> {
            if (initChildren) {
                initChildren(category);
            }
            categoryList.add(category);
        });

        return categoryList;
    }

    private static void initChildren(Category category) {
        Set<Category> childrenSet = category.getChildren();
        childrenSet.forEach(CategoryBdImpl::initChildren);
    }
}
