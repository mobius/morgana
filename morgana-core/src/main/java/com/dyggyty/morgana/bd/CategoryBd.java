package com.dyggyty.morgana.bd;

import com.dyggyty.morgana.model.category.Category;

import java.util.List;

public interface CategoryBd {

    public List<Category> findAll();

    public List<Category> findTopLevel();
}
