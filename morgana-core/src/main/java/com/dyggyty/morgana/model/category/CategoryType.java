package com.dyggyty.morgana.model.category;

public enum CategoryType {
    SINGLE_DOCUMENT, TABLE, ICONS
}
