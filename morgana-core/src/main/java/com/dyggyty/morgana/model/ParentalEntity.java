package com.dyggyty.morgana.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

@MappedSuperclass
public abstract class ParentalEntity<PARENT extends ParentalEntity<PARENT>> extends GenericEntity {

    @ManyToOne
    @JoinColumn(name = "parent", nullable = true)
    private PARENT parent;

    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    @Column(insertable = false, updatable = false)
    private Set<PARENT> children;

    public void setChildren(Set<PARENT> children) {
        this.children = children;
    }

    public Set<PARENT> getChildren() {
        return children;
    }

    public PARENT getParent() {
        return parent;
    }

    public void setParent(PARENT parent) {
        this.parent = parent;
    }
}
