package com.dyggyty.morgana.model.category;

import com.dyggyty.morgana.model.ParentalEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Contains category information
 */
@Entity
@Table(name = "categories")
public class Category extends ParentalEntity<Category> {

    @Column(name = "category_type", nullable = false)
    private CategoryType categoryType;

    @Column(name = "event_listener_class", nullable = true)
    private String eventListenerClass;

    public CategoryType getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(CategoryType categoryType) {
        this.categoryType = categoryType;
    }

    public String getEventListenerClass() {
        return eventListenerClass;
    }

    public void setEventListenerClass(String eventListenerClass) {
        this.eventListenerClass = eventListenerClass;
    }
}
