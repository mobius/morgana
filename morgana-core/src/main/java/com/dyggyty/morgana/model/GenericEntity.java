package com.dyggyty.morgana.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * Contains generic information for the entity
 */
@MappedSuperclass
public abstract class GenericEntity implements Serializable {

    @Id
    @GeneratedValue
    @Column
    private Long id;

    @Column(name = "name", length = 256)
    private String name;

    @Column(name = "display_name", length = 256)
    private String displayName;

    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date created = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date updated = new Date();

    @Column(name = "description", length = 4096)
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "GenericEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", displayName='" + displayName + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                ", description='" + description + '\'' +
                '}';
    }
}
