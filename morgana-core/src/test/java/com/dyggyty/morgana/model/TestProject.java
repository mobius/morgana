package com.dyggyty.morgana.model;

import com.dyggyty.morgana.configuration.MenuItem;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.Set;

/**
 * Contains project information
 */
@Entity
@Table(name = "project")
@MenuItem(value = "menu.projects", order = 2)
public class TestProject {

    @Id
    @GeneratedValue
    private Long id;

    /**
     * TestProject name.
     */
    private String name;

    /**
     * Date when project starts.
     */
    private Date startDate;

    /**
     * TestProject close date.
     */
    private Date finishDate;

    /**
     * date when project was created in the system.
     */
    private Date created;

    /**
     * Current resources testDemands for the project
     */
    @OneToMany(mappedBy = "testProject")
    private Set<TestDemand> testDemands;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Set<TestDemand> getTestDemands() {
        return testDemands;
    }

    public void setTestDemands(Set<TestDemand> testDemands) {
        this.testDemands = testDemands;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof TestProject)) return false;

        TestProject testProject = (TestProject) o;

        return new EqualsBuilder()
                .append(id, testProject.id)
                .append(name, testProject.name)
                .append(startDate, testProject.startDate)
                .append(finishDate, testProject.finishDate)
                .append(created, testProject.created)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(name)
                .append(startDate)
                .append(finishDate)
                .append(created)
                .toHashCode();
    }
}
