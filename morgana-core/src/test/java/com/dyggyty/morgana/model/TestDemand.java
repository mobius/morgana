package com.dyggyty.morgana.model;

import com.dyggyty.morgana.configuration.MenuItem;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Represents demand to resources for the testProject.
 */
@Entity
@Table(name = "demand")
@MenuItem(value = "menu.demands", order = 1)
public class TestDemand {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name="testProject")
    private TestProject testProject;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TestProject getTestProject() {
        return testProject;
    }

    public void setTestProject(TestProject testProject) {
        this.testProject = testProject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof TestDemand)) return false;

        TestDemand testDemand = (TestDemand) o;

        return new EqualsBuilder()
                .append(id, testDemand.id)
                .append(testProject, testDemand.testProject)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(testProject)
                .toHashCode();
    }
}
