package com.dyggyty.morgana.framework;

import com.dyggyty.morgana.dto.CollectionFieldDto;
import com.dyggyty.morgana.dto.DocumentFieldDto;
import com.dyggyty.morgana.dto.EntityDescriptorDto;
import com.dyggyty.morgana.entity.EntityDescriptorProvider;
import com.dyggyty.morgana.entity.FieldTemplate;
import com.dyggyty.morgana.model.TestProject;
import com.dyggyty.morgana.model.administration.Member;
import com.dyggyty.morgana.persistence.JpaEntityCrudRepositoryImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath*:/applicationContext.xml")
public class JpaEntityDescriptorProviderImplTest {

    @Autowired
    @Qualifier("jpaEntityDescriptorProviderImpl")
    private EntityDescriptorProvider entityDescriptorProvider;

    @Test
    public void testGetEntityDescriptorByClass() {

        EntityDescriptorDto entityDescriptorDto = entityDescriptorProvider.getEntityDescriptor(Member.class);

        assertNotNull(entityDescriptorDto);

        assertEquals("menu.users", entityDescriptorDto.getCategoryName());
        assertEquals("Member", entityDescriptorDto.getEntityName());
        assertEquals(Member.class, entityDescriptorDto.getEntityClass());
        assertEquals(JpaEntityCrudRepositoryImpl.class, entityDescriptorDto.getCrudRepository());

        List<DocumentFieldDto> documentFieldDtos = entityDescriptorDto.getDocumentFields();

        assertTrue("Document fields size should has 5 fields", documentFieldDtos.size() == 5);

        DocumentFieldDto loginDocumentField = documentFieldDtos.get(0);
        assertTrue(loginDocumentField.isId());
        assertFalse(loginDocumentField.isVersion());
        assertEquals("login", loginDocumentField.getName());
        assertEquals("label.user.login", loginDocumentField.getTitle());
        assertEquals(FieldTemplate.STRING, loginDocumentField.getTemplate());
        assertFalse(loginDocumentField.isHideOnForm());

        DocumentFieldDto emailDocumentField = documentFieldDtos.get(2);
        assertFalse(emailDocumentField.isId());
        assertFalse(emailDocumentField.isVersion());
        assertEquals("email", emailDocumentField.getName());
        assertEquals("label.email", emailDocumentField.getTitle());
        assertEquals(FieldTemplate.EMAIL, emailDocumentField.getTemplate());
        assertFalse(emailDocumentField.isHideOnForm());
        assertFalse(emailDocumentField.isHideOnTable());

        DocumentFieldDto passwordDocumentField = documentFieldDtos.get(3);
        assertFalse(passwordDocumentField.isId());
        assertFalse(passwordDocumentField.isVersion());
        assertEquals("password", passwordDocumentField.getName());
        assertEquals("label.password", passwordDocumentField.getTitle());
        assertEquals(FieldTemplate.PASSWORD, passwordDocumentField.getTemplate());
        assertFalse(passwordDocumentField.isHideOnForm());
        assertTrue(passwordDocumentField.isHideOnTable());
    }

    @Test
    public void testOneToMany() {
        EntityDescriptorDto entityDescriptorDto = entityDescriptorProvider.getEntityDescriptor(TestProject.class);

        List<CollectionFieldDto> tabs = entityDescriptorDto.getDocumentFieldTabs();
        assertNotNull("Tabs must not be empty", tabs);
        assertTrue("Must be only one tab", tabs.size() == 1);
        assertEquals("Must be one-to-many tab", CollectionFieldDto.class, tabs.get(0).getClass());
    }
}
