![logo_135.png](https://bitbucket.org/repo/xeR6bn/images/2599642921-logo_135.png)

***
# Build Process
## Preconditions:
1. Must be installed and running MySQL
1. Must be installed tomcat 8.0.24 
1. Must be installed maven 3
1. Maven bin folder must be in the PATH system variable

##Build process:
1. Run **mvn clean package** command  

##Run process:
1. Create database in the MySQL
1. Add JNDI resource to the $CATALINA_HOME/conf/context.xml file: *<Resource name="jdbc/MorganaDB" auth="Container" type="javax.sql.DataSource" maxTotal="100" maxIdle="30" maxWaitMillis="10000" username="developer" password="developer" driverClassName="com.mysql.jdbc.Driver" validationQuery="select 1" testOnBorrow="true" url="jdbc:mysql://localhost:3306/morgana"/>*
1. Add JNDI reference to the web.xml: *<ResourceLink name="jdbc/MorganaDB" global="jdbc/MorganaDB" type="javax.sql.DataSource"/>*
1. Copy MySQL connector JAR file to the $CATALINA_HOME/lib