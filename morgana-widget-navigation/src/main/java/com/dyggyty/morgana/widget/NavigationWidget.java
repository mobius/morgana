package com.dyggyty.morgana.widget;

import com.dyggyty.morgana.configuration.WidgetConfiguration;
import com.dyggyty.morgana.framework.EntityDefaultFunction;
import com.dyggyty.morgana.framework.MorganaRequest;
import com.dyggyty.morgana.framework.PluginFunctionFactory;
import com.dyggyty.morgana.framework.Widget;
import com.dyggyty.morgana.framework.function.PluginFunction;
import com.dyggyty.morgana.web.utils.HttpUtils;
import org.apache.commons.collections4.map.ListOrderedMap;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Breadcrumbs widget
 */
@ManagedBean(name = NavigationWidget.WIDGET_NAME)
@RequestScoped
@WidgetConfiguration(value = NavigationWidget.WIDGET_NAME, position = WidgetConfiguration.Position.BODY_TOP)
public class NavigationWidget implements Widget {

    public static final String WIDGET_NAME = "navigation";

    @Autowired
    private MessageSource messages;

    @Autowired
    private PluginFunctionFactory pluginFunctionFactory;

    public MenuModel getBreadcrumbModel() {

        MenuModel menuModel = new DefaultMenuModel();

        DefaultMenuItem index = new DefaultMenuItem();
        index.setValue("Index");
        index.setAjax(false);
        index.setHref("/");
        menuModel.addElement(index);

        ListOrderedMap<Class<? extends PluginFunction>, MorganaRequest> navigation = HttpUtils.getNavigation();

        navigation.forEach((pluginFunctionClass, morganaRequest) -> {
            Map<String, String[]> parameters = navigation.get(pluginFunctionClass).getParameters();
            Map<String, List<String>> menuItemParameters = new HashMap<>();
            parameters.forEach((paramName, paramValues) -> {
                List<String> parameterValues = new ArrayList<>();
                Collections.addAll(parameterValues, paramValues);
                menuItemParameters.put(paramName, parameterValues);
            });

            String menuItemTitle = getMenuItemTitle(morganaRequest, pluginFunctionClass);
            DefaultMenuItem menuItem = new DefaultMenuItem(menuItemTitle);
            menuItem.setTitle(menuItemTitle);

            menuItem.setAjax(false);
            menuItem.setParams(menuItemParameters);

            menuItem.setCommand("#{pluginProvider.executeFunction}");

            menuModel.addElement(menuItem);
        });

        menuModel.generateUniqueIds();
        return menuModel;
    }

    private String getMenuItemTitle(MorganaRequest morganaRequest, Class<? extends PluginFunction> pluginFunctionClass) {
        EntityDefaultFunction entityDefaultFunction =
                pluginFunctionClass.getAnnotation(EntityDefaultFunction.class);
        Map.Entry<String, ? extends PluginFunction> pluginFunctionEntry
                = pluginFunctionFactory.getPluginFunction(pluginFunctionClass);

        String functionName = pluginFunctionEntry.getValue().getFunctionName();

        Locale browserLocale = HttpUtils.getBrowserLocale();
        String title = entityDefaultFunction == null ? functionName : entityDefaultFunction.title();
        return messages.getMessage(title, new Object[]{}, title, browserLocale);
    }
}
