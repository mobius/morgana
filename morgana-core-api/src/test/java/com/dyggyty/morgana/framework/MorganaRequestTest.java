package com.dyggyty.morgana.framework;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Test for the MorganaRequest class
 */

public class MorganaRequestTest {

    private static final String TEST_PARAM = "testParam";

    @Test
    public void getParameter() {
        Map<String, String[]> requestParams = new HashMap<>();
        MorganaRequest request = new MorganaRequest(requestParams);

        Assert.assertNull("Should be no any parameters", request.getParameter(TEST_PARAM));
        Assert.assertTrue("Should be no any parameters", request.getParameters().size() == 0);

        //testing when original parameters map was modified.
        requestParams.put(TEST_PARAM, new String[]{TEST_PARAM});
        Assert.assertTrue("Should be no any parameters", request.getParameters().size() == 0);

        //Testing with existing parameters
        request = new MorganaRequest(requestParams);

        Assert.assertEquals(TEST_PARAM, ((String[])request.getParameter(TEST_PARAM))[0]);
        Assert.assertTrue("Should contains only one value", request.getParameters().size() == 1);
    }
}
