package com.dyggyty.morgana.configuration;

import org.springframework.stereotype.Component;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface WidgetConfiguration {

    String APPLY_TO_ALL_EXPR = ".*";

    enum Position{
        HEADER("header"), LEFT_PANE("left_pane"), RIGHT_PANE("right_pane"), BODY_TOP("body_top"),
        BODY_BOTTOM("body_bottom"), FOOTER("foter");

        private String value;

        Position(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    String value();

    String applyToExpr() default APPLY_TO_ALL_EXPR;

    Position position() default Position.BODY_TOP;

    int order() default 0;
}
