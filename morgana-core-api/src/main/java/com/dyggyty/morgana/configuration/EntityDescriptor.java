package com.dyggyty.morgana.configuration;

import com.dyggyty.morgana.persistence.DefaultRepository;
import com.dyggyty.morgana.persistence.EntityCrudRepository;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface EntityDescriptor {

    /**
     * Entity title
     */
    String value();

    /**
     * Title on the list.
     */
    String listFormTitle() default "";

    /**
     * List of available action.
     *
     * @return Actions array.
     */
    EntityAction[] actions() default {};

    /**
     * Entity repository for the CRUD operations.
     *
     * @return Repository class.
     */
    Class<? extends EntityCrudRepository> repositoryClass() default DefaultRepository.class;
}
