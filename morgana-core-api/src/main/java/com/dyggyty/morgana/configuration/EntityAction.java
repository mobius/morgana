package com.dyggyty.morgana.configuration;

import com.dyggyty.morgana.framework.function.PluginFunction;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Indicates that entity has actions
 */
@Retention(RUNTIME)
public @interface EntityAction {

    /**
     * Function class.
     *
     * @return Class of the function will be executed.
     */
    Class<? extends PluginFunction> function();

    /**
     * Action image.
     *
     * @return Action image class path..
     */
    String image() default "";

    /**
     * Action title.
     *
     * @return String key or value for action title.
     */
    String title() default "";

    /**
     * Action description.
     *
     * @return String key or value for action description.
     */
    String description() default "";

    /**
     * Sets action visible on entity list.
     *
     * @return Action visibility flag.
     */
    boolean visibleOnList() default true;

    /**
     * Sets action visible on entity form.
     *
     * @return Action visibility flag.
     */
    boolean visibleOnForm() default true;

    /**
     * Default action position
     *
     * @return Action position.
     */
    byte order() default Byte.MAX_VALUE;
}
