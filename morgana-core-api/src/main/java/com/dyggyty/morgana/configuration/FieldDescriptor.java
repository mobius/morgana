package com.dyggyty.morgana.configuration;

import com.dyggyty.morgana.entity.FieldTemplate;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FieldDescriptor {

    String value() default "";

    boolean hideOnTable() default false;

    boolean hideOnForm() default false;

    int order() default 0;

    FieldTemplate template() default FieldTemplate.AUTO;
}
