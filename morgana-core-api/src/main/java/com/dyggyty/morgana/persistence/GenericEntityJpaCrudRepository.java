package com.dyggyty.morgana.persistence;

import com.dyggyty.morgana.utils.ReflectionUtils;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.hibernate.metadata.ClassMetadata;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * Common entity CRUD repository for the specific class.
 */
@Transactional(readOnly = true)
public class GenericEntityJpaCrudRepository<ENTITY, KEY extends Serializable>
        implements EntityCrudRepository<ENTITY, KEY> {

    @PersistenceContext
    private EntityManager entityManager;

    private SimpleJpaRepository<ENTITY, KEY> simpleJpaRepository;
    private JpaEntityInformation<ENTITY, ?> jpaEntityInformation;
    private Class<ENTITY> domainClass;
    private ClassMetadata classMetadata;

    @PostConstruct
    public void init() {
        Type[] arguments = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments();

        //noinspection unchecked
        domainClass = (Class<ENTITY>) arguments[0];

        jpaEntityInformation = JpaEntityInformationSupport.getMetadata(domainClass, entityManager);
        simpleJpaRepository = new SimpleJpaRepository<>(jpaEntityInformation, entityManager);

        HibernateEntityManagerFactory entityManagerFactory =
                (HibernateEntityManagerFactory) entityManager.getEntityManagerFactory();
        classMetadata = entityManagerFactory.getSessionFactory().getClassMetadata(domainClass);
    }

    @Override
    public void delete(KEY id) {
        simpleJpaRepository.delete(id);
    }

    @Override
    public void deleteByString(String id) {
        @SuppressWarnings("unchecked")
        KEY identifier = (KEY) ReflectionUtils.getFieldValue(classMetadata.getIdentifierType(), id);
        delete(identifier);
    }

    @Override
    public void delete(ENTITY entity) {
        simpleJpaRepository.delete(entity);
    }

    @Override
    public void delete(Iterable<ENTITY> entities) {
        simpleJpaRepository.delete(entities);
    }

    @Override
    public void deleteInBatch(Iterable<ENTITY> entities) {
        simpleJpaRepository.deleteInBatch(entities);
    }

    @Override
    public void deleteAll() {
        simpleJpaRepository.deleteAll();
    }

    @Override
    public ENTITY findOne(KEY id) {
        return simpleJpaRepository.findOne(id);
    }

    @Override
    public ENTITY findOne(String id) {
        @SuppressWarnings("unchecked")
        KEY identifier = (KEY) ReflectionUtils.getFieldValue(classMetadata.getIdentifierType(), id);
        return findOne(identifier);
    }

    @Override
    public List<ENTITY> findAll() {
        return simpleJpaRepository.findAll();
    }

    @Override
    public List<ENTITY> findAll(Sort sort) {
        return simpleJpaRepository.findAll(sort);
    }

    @Override
    public Page<ENTITY> findAll(Pageable pageable) {
        return simpleJpaRepository.findAll(pageable);
    }

    @Override
    public long count() {
        return simpleJpaRepository.count();
    }

    @Override
    public ENTITY save(Map<String, String> entity) {
        return null;
    }

    @Override
    public ENTITY save(ENTITY entity) {
        return simpleJpaRepository.save(entity);
    }

    @Override
    public List<ENTITY> save(Iterable<ENTITY> entities) {
        return simpleJpaRepository.save(entities);
    }

    protected ClassMetadata getClassMetadata() {
        return classMetadata;
    }

    protected Class<ENTITY> getDomainClass() {
        return domainClass;
    }

    protected JpaEntityInformation<ENTITY, ?> getJpaEntityInformation() {
        return jpaEntityInformation;
    }

    protected SimpleJpaRepository<ENTITY, KEY> getSimpleJpaRepository() {
        return simpleJpaRepository;
    }

    protected EntityManager getEntityManager() {
        return entityManager;
    }
}
