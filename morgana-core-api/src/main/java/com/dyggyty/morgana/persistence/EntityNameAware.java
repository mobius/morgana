package com.dyggyty.morgana.persistence;

/**
 * Allow to setup entity name into the CRUD repository.
 */
public interface EntityNameAware {

    /**
     * Setup registered entity name.
     *
     * @param entityName Name of the entity.
     */
    void setEntityName(String entityName);
}
