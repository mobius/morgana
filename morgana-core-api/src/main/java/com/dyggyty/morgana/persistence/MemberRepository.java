package com.dyggyty.morgana.persistence;

import com.dyggyty.morgana.model.administration.Member;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Sergey Kovalev
 */
@Repository
public interface MemberRepository extends PagingAndSortingRepository<Member, String> {
}
