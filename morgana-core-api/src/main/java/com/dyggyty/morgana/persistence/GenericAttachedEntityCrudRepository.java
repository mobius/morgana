package com.dyggyty.morgana.persistence;

import com.dyggyty.morgana.model.AttachedEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Generic CRUD repository for attached entity
 */
public abstract class GenericAttachedEntityCrudRepository<ENTITY extends AttachedEntity>
        extends GenericEntityJpaCrudRepository<ENTITY, Long> implements AttachedEntityCrudRepository<ENTITY> {

private static final String PARAMETER_MAPPED_ENTITY_ID = "mappedEntityId";
private static final String PARAMETER_MAPPED_ENTITY_TYPE = "mappedEntityType";

    private Class<ENTITY> typeOfEntity;

    @SuppressWarnings("unchecked")
    public GenericAttachedEntityCrudRepository() {
        this.typeOfEntity = (Class<ENTITY>)
                ((ParameterizedType) getClass()
                        .getGenericSuperclass())
                        .getActualTypeArguments()[0];
    }

    @Override
    public List<ENTITY> getAttachedEntities(String entityId, String entityName) {

        EntityManager entityManager = getEntityManager();
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

        CriteriaQuery<ENTITY> attachedEntityQuery = cb.createQuery(typeOfEntity);
        Root<ENTITY> entityRoot = attachedEntityQuery.from(typeOfEntity);
        attachedEntityQuery.select(entityRoot);

        ParameterExpression<String> mappedEntityIdParameter = cb.parameter(String.class);
        ParameterExpression<String> mappedEntityNameParameter = cb.parameter(String.class);
        attachedEntityQuery.where(
                cb.equal(entityRoot.get(PARAMETER_MAPPED_ENTITY_ID), mappedEntityIdParameter),
                cb.equal(entityRoot.get(PARAMETER_MAPPED_ENTITY_TYPE), mappedEntityNameParameter)
                );

        TypedQuery<ENTITY> query = entityManager.createQuery(attachedEntityQuery);
        query.setParameter(mappedEntityIdParameter, entityId);
        query.setParameter(mappedEntityNameParameter, entityName);

        return query.getResultList();
    }
}
