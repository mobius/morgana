package com.dyggyty.morgana.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Provides CRUD operations for the entity
 */
public interface EntityCrudRepository<T, KEY extends Serializable> {

    /**
     * @see org.springframework.data.repository.CrudRepository#delete(java.io.Serializable)
     */
    void delete(KEY id);

    /**
     * @param id String representation of the identifier.
     * @see org.springframework.data.repository.CrudRepository#delete(java.io.Serializable)
     */
    void deleteByString(String id);

    /**
     * @see org.springframework.data.repository.CrudRepository#delete(java.lang.Object)
     */
    void delete(T entity);

    /**
     * @see org.springframework.data.repository.CrudRepository#delete(java.lang.Iterable)
     */
    void delete(Iterable<T> entities);

    /**
     * @see org.springframework.data.jpa.repository.JpaRepository#deleteInBatch(java.lang.Iterable)
     */
    void deleteInBatch(Iterable<T> entities);

    /**
     * @see CrudRepository#deleteAll() eleteAll()
     */
    void deleteAll();

    /**
     * @see org.springframework.data.repository.CrudRepository#findOne(java.io.Serializable)
     */
    T findOne(KEY id);

    /**
     * @param id String representation of the identifier.
     * @see org.springframework.data.repository.CrudRepository#findOne(java.io.Serializable)
     */
    T findOne(String id);

    /**
     * @see org.springframework.data.jpa.repository.JpaRepository#findAll()
     */
    List<T> findAll();

    /**
     * @see org.springframework.data.jpa.repository.JpaRepository#findAll(org.springframework.data.domain.Sort)
     */
    List<T> findAll(Sort sort);

    /**
     * @see org.springframework.data.repository.PagingAndSortingRepository#findAll(org.springframework.data.domain.Pageable)
     */
    Page<T> findAll(Pageable pageable);

    /**
     * @see org.springframework.data.repository.CrudRepository#count()
     */
    long count();

    /**
     * Save the entity from mapped values.
     *
     * @param entity Map of the field and values.
     * @return Saved entity
     * @see org.springframework.data.repository.CrudRepository#save(java.lang.Object)
     */
    T save(Map<String, String> entity);

    /**
     * @see org.springframework.data.repository.CrudRepository#save(java.lang.Object)
     */
    T save(T entity);

    /**
     * @see org.springframework.data.jpa.repository.JpaRepository#save(java.lang.Iterable)
     */
    List<T> save(Iterable<T> entities);
}
