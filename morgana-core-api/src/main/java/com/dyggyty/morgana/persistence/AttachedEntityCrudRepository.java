package com.dyggyty.morgana.persistence;

import com.dyggyty.morgana.model.AttachedEntity;

import java.util.List;

/**
 * CRUD repository for attached entities
 */
public interface AttachedEntityCrudRepository<T extends AttachedEntity> extends EntityCrudRepository<T, Long> {

    /**
     * Return list of attached entities
     *
     * @param entityId   Main entity identifier string representation.
     * @param entityName Name of the main entity.
     * @return List of attached entities.
     */
    List<T> getAttachedEntities(String entityId, String entityName);
}
