package com.dyggyty.morgana.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Indicates that default repository must be used.
 */
public class DefaultRepository implements EntityCrudRepository {
    @Override
    public void delete(Serializable id) {
        throw new NotImplementedException();
    }

    @Override
    public void deleteByString(String id) {
        throw new NotImplementedException();
    }

    @Override
    public void delete(Object entity) {
        throw new NotImplementedException();
    }

    @Override
    public void delete(Iterable entities) {
        throw new NotImplementedException();
    }

    @Override
    public void deleteInBatch(Iterable entities) {
        throw new NotImplementedException();
    }

    @Override
    public void deleteAll() {
        throw new NotImplementedException();
    }

    @Override
    public Object findOne(Serializable id) {
        throw new NotImplementedException();
    }

    @Override
    public Object findOne(String id) {
        throw new NotImplementedException();
    }

    @Override
    public List findAll() {
        throw new NotImplementedException();
    }

    @Override
    public List findAll(Sort sort) {
        throw new NotImplementedException();
    }

    @Override
    public Page findAll(Pageable pageable) {
        throw new NotImplementedException();
    }

    @Override
    public long count() {
        throw new NotImplementedException();
    }

    @Override
    public Object save(Object entity) {
        throw new NotImplementedException();
    }

    @Override
    public List save(Iterable entities) {
        throw new NotImplementedException();
    }

    @Override
    public Object save(Map entity) {
        throw new NotImplementedException();
    }
}
