package com.dyggyty.morgana.dto;

import java.lang.reflect.Member;

/**
 * Contains generic information about field.
 */
public abstract class AbstractDocumentFieldDto<T extends AbstractDocumentFieldDto> implements Comparable<T> {

    private int order;
    private String title;
    private boolean hideOnForm;
    private String name;
    private final Member member;
    private String bindableType;


    public AbstractDocumentFieldDto(Member member, String bindableType) {
        this.member = member;
        this.bindableType = bindableType;
    }

    public Member getMember() {
        return member;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getTitle() {
        return title == null ? getName() : title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getOrder() {
        return order;
    }

    public void setHideOnForm(boolean hideOnForm) {
        this.hideOnForm = hideOnForm;
    }

    public boolean isHideOnForm() {
        return hideOnForm;
    }

    public String getBindableType() {
        return bindableType;
    }

    public abstract boolean isHideOnTable();

    protected int compareField(T documentFieldDto) {
        return 0;
    }

    @Override
    public int compareTo(T documentFieldDto) {

        int result = Integer.compare(getOrder(), documentFieldDto.getOrder());

        if (result != 0) {
            return result;
        }

        result = compareField(documentFieldDto);

        if (result != 0) {
            return result;
        }

        return getTitle().compareTo(documentFieldDto.getTitle());
    }
}
