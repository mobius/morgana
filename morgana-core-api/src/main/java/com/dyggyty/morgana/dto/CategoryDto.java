package com.dyggyty.morgana.dto;

import java.util.List;
import java.util.Map;

/**
 * Dto class that represents category object.
 */
public class CategoryDto implements Comparable<CategoryDto>{

    /**
     * Category name key.<br/>
     * Can use localizations.
     */
    private String name;

    /**
     * Menu item position.
     */
    private int order;

    private String pluginName;

    private String pluginFunctionName;

    private Map<String, String> arguments;

    /**
     * Children for parent.
     */
    private List<CategoryDto> children;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CategoryDto> getChildren() {
        return children;
    }

    public void setChildren(List<CategoryDto> children) {
        this.children = children;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getPluginName() {
        return pluginName;
    }

    public void setPluginName(String pluginName) {
        this.pluginName = pluginName;
    }

    public String getPluginFunctionName() {
        return pluginFunctionName;
    }

    public void setPluginFunctionName(String pluginFunctionName) {
        this.pluginFunctionName = pluginFunctionName;
    }

    public Map<String, String> getArguments() {
        return arguments;
    }

    public void setArguments(Map<String, String> arguments) {
        this.arguments = arguments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryDto that = (CategoryDto) o;

        return name.equals(that.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "CategoryDto{" +
                "name='" + name + '\'' +
                ", order=" + order +
                ", pluginName='" + pluginName + '\'' +
                ", pluginFunctionName='" + pluginFunctionName + '\'' +
                '}';
    }

    @Override
    public int compareTo(CategoryDto o) {
        int result =  Integer.compare(order, o.order);

        if (result == 0) {
            result = name.compareTo(o.name);
        }

        return result;
    }
}
