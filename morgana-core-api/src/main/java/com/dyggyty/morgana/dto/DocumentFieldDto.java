package com.dyggyty.morgana.dto;

import com.dyggyty.morgana.entity.FieldTemplate;

import java.lang.reflect.Member;

/**
 * Provide information about entity field.
 */
public class DocumentFieldDto extends AbstractDocumentFieldDto<DocumentFieldDto> {

    private boolean id;
    private final FieldTemplate template;
    private final boolean version;
    private int length;
    private final boolean hideOnTable;

    public DocumentFieldDto(Member member, String bindableType, FieldTemplate template, int length, boolean hideOnTable,
                            boolean id, boolean version) {
        super(member, bindableType);

        this.template = template;
        this.hideOnTable = hideOnTable;
        this.id = id;
        this.version = version;
        this.length = length;
    }

    public boolean isId() {
        return id;
    }

    public boolean isVersion() {
        return version;
    }

    public int getLength() {
        return length;
    }

    public boolean isHideOnTable() {
        return hideOnTable;
    }

    public FieldTemplate getTemplate() {
        return template;
    }

    protected int compareField(DocumentFieldDto documentFieldDto) {

        //Identifier should be first
        int result = Boolean.compare(documentFieldDto.id, id);

        if (result != 0) {
            return result;
        }

        //version should be last
        return Boolean.compare(version, documentFieldDto.version);
    }
}
