package com.dyggyty.morgana.dto;

import java.lang.reflect.Member;

public class CollectionFieldDto extends AbstractDocumentFieldDto<CollectionFieldDto> {

    private Member referencedField;

    public CollectionFieldDto(Member member, String bindableType) {
        super(member, bindableType);
    }

    @Override
    public boolean isHideOnTable() {
        return true;
    }

    public Member getReferencedField() {
        return referencedField;
    }

    public void setReferencedField(Member referencedField) {
        this.referencedField = referencedField;
    }
}
