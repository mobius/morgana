package com.dyggyty.morgana.dto;

public class EntityActionDto implements Comparable<EntityActionDto> {

    private String pluginName;
    private String functionName;
    private String image;
    private String title;
    private String description;
    private boolean visibleOnList;
    private boolean visibleOnForm;
    private byte order;

    public EntityActionDto(String pluginName, String functionName) {
        this.pluginName = pluginName;
        this.functionName = functionName;
    }

    public String getPluginName() {
        return pluginName;
    }

    public String getFunctionName() {
        return functionName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isVisibleOnList() {
        return visibleOnList;
    }

    public void setVisibleOnList(boolean visibleOnList) {
        this.visibleOnList = visibleOnList;
    }

    public boolean isVisibleOnForm() {
        return visibleOnForm;
    }

    public void setVisibleOnForm(boolean visibleOnForm) {
        this.visibleOnForm = visibleOnForm;
    }

    public byte getOrder() {
        return order;
    }

    public void setOrder(byte order) {
        this.order = order;
    }

    @Override
    public int compareTo(EntityActionDto o) {
        if (o == null) {
            return -1;
        }
        return Byte.compare(order, o.order);
    }
}
