package com.dyggyty.morgana.dto;

import com.dyggyty.morgana.model.AttachedEntity;

/**
 * Attached entity descriptor class.
 */
public class AttachedEntityDescriptorDto<T extends AttachedEntity> extends GenericEntityDescriptorDto<T> {

    /**
     * Indicate is it possible to contain more than one entity extension of this type.
     */
    private boolean multiple;

    /**
     * Indicates is it possible to edit attached entity.
     */
    private boolean readOnly;

    public boolean isMultiple() {
        return multiple;
    }

    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }
}
