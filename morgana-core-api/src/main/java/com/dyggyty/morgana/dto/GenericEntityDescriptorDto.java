package com.dyggyty.morgana.dto;

import com.dyggyty.morgana.persistence.EntityCrudRepository;

import java.util.List;

/**
 * Generic entity descriptor class.
 *
 * @param <T> Type of the entity.
 */
public abstract class GenericEntityDescriptorDto<T> {

    private String entityTitle;

    /**
     * Registered in the EntityCRUDRepository name of the entity.
     */
    private String entityName;
    private Class<T> entityClass;
    private List<DocumentFieldDto> documentFields;
    private Class<? extends EntityCrudRepository> crudRepository;

    public String getEntityTitle() {
        return entityTitle;
    }

    public void setEntityTitle(String entityTitle) {
        this.entityTitle = entityTitle;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public Class<T> getEntityClass() {
        return entityClass;
    }

    public void setEntityClass(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public List<DocumentFieldDto> getDocumentFields() {
        return documentFields;
    }

    public void setDocumentFields(List<DocumentFieldDto> documentFields) {
        this.documentFields = documentFields;
    }

    public Class<? extends EntityCrudRepository> getCrudRepository() {
        return crudRepository;
    }

    public void setCrudRepository(Class<? extends EntityCrudRepository> crudRepository) {
        this.crudRepository = crudRepository;
    }
}
