package com.dyggyty.morgana.dto;

import java.util.List;

public class EntityDescriptorDto<T> extends GenericEntityDescriptorDto<T> {

    private String categoryName;
    private String listFormTitle;
    private List<EntityActionDto> entityActionDtos;
    private CategoryDto parentalCategory;
    private int order;
    private List<AttachedEntityDescriptorDto> attachedEntityDescriptorDtos;
    private List<CollectionFieldDto> documentFieldTabs;

    public String getListFormTitle() {
        return listFormTitle;
    }

    public void setListFormTitle(String listFormTitle) {
        this.listFormTitle = listFormTitle;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public CategoryDto getParentalCategory() {
        return parentalCategory;
    }

    public void setParentalCategory(CategoryDto parentalCategory) {
        this.parentalCategory = parentalCategory;
    }

    public List<EntityActionDto> getEntityActionDtos() {
        return entityActionDtos;
    }

    public void setEntityActionDtos(List<EntityActionDto> entityActionDtos) {
        this.entityActionDtos = entityActionDtos;
    }

    public List<AttachedEntityDescriptorDto> getAttachedEntityDescriptorDtos() {
        return attachedEntityDescriptorDtos;
    }

    public void setAttachedEntityDescriptorDtos(List<AttachedEntityDescriptorDto> attachedEntityDescriptorDtos) {
        this.attachedEntityDescriptorDtos = attachedEntityDescriptorDtos;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public List<CollectionFieldDto> getDocumentFieldTabs() {
        return documentFieldTabs;
    }

    public void setDocumentFieldTabs(List<CollectionFieldDto> documentFieldTabs) {
        this.documentFieldTabs = documentFieldTabs;
    }
}
