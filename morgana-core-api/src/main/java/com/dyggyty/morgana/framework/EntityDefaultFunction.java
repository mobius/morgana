package com.dyggyty.morgana.framework;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Describes function.<br/>
 * Required when function will be displayed by default for the entity without declaration.
 */
@Target({TYPE})
@Retention(RUNTIME)
public @interface EntityDefaultFunction {

    /**
     * Action image.
     *
     * @return Action image class path..
     */
    String image() default "ui-icon-document";

    /**
     * Action title.
     *
     * @return String key or value for action title.
     */
    String title();

    /**
     * Action description.
     *
     * @return String key or value for action description.
     */
    String description() default "";

}
