package com.dyggyty.morgana.framework;

import com.dyggyty.morgana.dto.CategoryDto;

import javax.naming.ConfigurationException;
import java.util.Set;

/**
 * Contains list of functions for the category provider.
 */
public interface CategoryProvider {

    /**
     * Return all categories tree.
     *
     * @return Category tree.
     */
    Set<CategoryDto> getCategories() throws ConfigurationException;
}
