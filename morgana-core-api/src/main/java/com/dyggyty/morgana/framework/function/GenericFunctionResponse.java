package com.dyggyty.morgana.framework.function;

import java.util.Map;

/**
 * Contains information with function work results.
 */
public abstract class GenericFunctionResponse implements FunctionResponse {

    private FunctionResponseType responseType;
    private String view;
    private Map<String, String[]> requestArguments;
    private Class<? extends PluginFunction> pluginFunctionClass;

    /**
     * Creates response to the specified function with modified parameters.
     *
     * @param pluginFunctionClass Plugin function class.
     * @param requestArguments    Arguments will be modified.
     */
    public GenericFunctionResponse(Class<? extends PluginFunction> pluginFunctionClass,
                                   Map<String, String[]> requestArguments) {
        this.pluginFunctionClass = pluginFunctionClass;
        this.requestArguments = requestArguments;
        responseType = FunctionResponseType.NEXT;
    }

    /**
     * Creates forward response to the specified function.
     *
     * @param pluginFunctionClass Plugin function class.
     */
    public GenericFunctionResponse(Class<? extends PluginFunction> pluginFunctionClass) {
        this.pluginFunctionClass = pluginFunctionClass;
        responseType = FunctionResponseType.FORWARD;
    }

    /**
     * Creates response to the previous function.
     */
    public GenericFunctionResponse() {
        responseType = FunctionResponseType.BACK;
    }

    /**
     * Creates response to specified page.
     *
     * @param view Name of the page.
     */
    public GenericFunctionResponse(String view) {
        this.view = view;
        responseType = FunctionResponseType.PAGE;
    }

    public String getView() {
        return view;
    }

    public FunctionResponseType getResponseType() {
        return responseType;
    }

    public Class<? extends PluginFunction> getPluginFunctionClass() {
        return pluginFunctionClass;
    }

    public Map<String, String[]> getRequestArguments() {
        return requestArguments;
    }

}
