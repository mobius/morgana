package com.dyggyty.morgana.framework;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Contains request data.
 */
public class MorganaRequest implements Serializable {

    private final Map<String, String[]> request;
    private final MorganaSession session;
    private final Object entityForm;

    /**
     * Creates new Morgana request.
     *
     * @param requestParams Request attributes
     */
    public MorganaRequest(Map<String, String[]> requestParams) {
        this(null, requestParams);
    }

    /**
     * Creates new Morgana request.
     *
     * @param sessionAttributes Session attributes.
     * @param requestParams     Request attributes
     */
    public MorganaRequest(Map<String, Object> sessionAttributes, Map<String, String[]> requestParams) {
        this(sessionAttributes, requestParams, null);

    }

    /**
     * Creates new Morgana request.
     *
     * @param sessionAttributes Session attributes.
     * @param requestParams     Request attributes.
     * @param entityForm        Entity form if exists.
     */
    public MorganaRequest(Map<String, Object> sessionAttributes, Map<String, String[]> requestParams, Object entityForm) {
        this.session = new MorganaSession(sessionAttributes);
        this.request =
                Collections.unmodifiableMap(requestParams == null ? new HashMap<>() : new HashMap<>(requestParams));
        this.entityForm = entityForm;
    }

    public MorganaSession getSession() {
        return session;
    }

    public Map<String, String[]> getParameters() {
        return request;
    }

    /**
     * Get single parameter by key.
     *
     * @param key Parameter name.
     * @return Parameter single value.
     */
    public Object getParameter(String key) {
        return request.get(key);
    }

    /**
     * Indicates if request contains specified parameter.
     *
     * @param key Parameter name.
     * @return <b>TRUE</b> if request contains key and <b>FALSE</b> otherwise
     */
    public boolean containsKey(String key) {
        return request.containsKey(key);
    }

    public Object getEntityForm() {
        return entityForm;
    }
}
