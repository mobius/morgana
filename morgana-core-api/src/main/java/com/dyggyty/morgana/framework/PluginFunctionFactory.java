package com.dyggyty.morgana.framework;

import com.dyggyty.morgana.dto.EntityActionDto;
import com.dyggyty.morgana.framework.function.PluginFunction;

import java.util.List;
import java.util.Map.Entry;

/**
 * Plugin function factory
 */
public interface PluginFunctionFactory {

    /**
     * Retrieve plugin.
     *
     * @param plugin Name of the plugin.
     * @return Initialized function.
     */
    Plugin getPlugin(String plugin);

    /**
     * Retrieve or creates new plugin function.
     *
     * @param plugin  Plugin instance.
     * @param function Function name,
     * @return Initialized function.
     */
    PluginFunction getPluginFunction(Plugin plugin, String function);

    /**
     * Retrieve plugin function by type.
     *
     * @param pluginFunctionClass Plugin function class.
     * @param <T>                 Type of the function.
     * @return Pair of plugin name and function instance,
     */
    <T extends PluginFunction> Entry<String, T> getPluginFunction(Class<T> pluginFunctionClass);

    /**
     * Get entity default actions.
     *
     * @param entityType Type of the entity.
     * @return List of action DTOs.
     */
    List<EntityActionDto> getDefaultActions(Class entityType);
}
