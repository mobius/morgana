package com.dyggyty.morgana.framework.function;

import java.util.Map;

public interface FunctionResponse {

    /**
     * Return page will be displayed on the body
     *
     * @return Body page.
     */
    public String getView();

    /**
     * Return type of the function.
     *
     * @return Function type.
     */
    public FunctionResponseType getResponseType();

    /**
     * Return plugin class that function belongs to.
     *
     * @return Plugin implementation class
     */
    public Class<? extends PluginFunction> getPluginFunctionClass();

    /**
     * Return argument that will be modified for the future functions.
     *
     * @return
     */
    public Map<String, String[]> getRequestArguments();
}
