package com.dyggyty.morgana.framework.function;

import com.dyggyty.morgana.framework.Plugin;

/**
 * Contains generic function functionality
 */
public abstract class GenericPluginFunction implements PluginFunction {

    private Plugin plugin;

    @Override
    public <T> boolean isDefaultOnEntityList(Class<T> entityClass) {
        return false;
    }

    @Override
    public <T> boolean isDefaultOnEntityForm(Class<T> entityClass) {
        return false;
    }

    @Override
    public void setPlugin(Plugin plugin){
        this.plugin = plugin;
    }

    protected Plugin getPlugin() {
        return plugin;
    }
}
