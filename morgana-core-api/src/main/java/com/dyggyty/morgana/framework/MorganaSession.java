package com.dyggyty.morgana.framework;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Contains session data
 */
public class MorganaSession implements Serializable {

    private final Map<String, Object> sessionAttributes;
    private final Set<String> deletedAttributes = Collections.synchronizedSet(new HashSet<>());

    MorganaSession() {
        this(null);
    }

    MorganaSession(Map<String, Object> sessionAttributes) {
        this.sessionAttributes =
                Collections.synchronizedMap(sessionAttributes == null ? new HashMap<>() : sessionAttributes);
    }

    /**
     * Return attribute value of null if attribute is not in the session.
     *
     * @param attributeKey Key of the attribute.
     * @param <T>          Type of the returned object.
     * @return Session object.
     */
    public <T> T getAttribute(String attributeKey) {
        Object attribute;

        if (deletedAttributes.contains(attributeKey)) {
            return null;
        }

        attribute = sessionAttributes.get(attributeKey);

        if (attribute == null) {
            return null;
        }

        //noinspection unchecked
        return (T) attribute;
    }

    public Map<String, Object> getSessionAttributes() {
        return Collections.unmodifiableMap(sessionAttributes);
    }

    public Set<String> getDeletedAttributes() {
        return Collections.unmodifiableSet(deletedAttributes);
    }

    public void deleteAttribute(String key) {
        sessionAttributes.remove(key);
        deletedAttributes.add(key);
    }
}
