package com.dyggyty.morgana.framework.function;

/**
 * Provides entity for editing.
 */
public interface EntityProviderFunctionResponse extends FunctionResponse {

    /**
     * Retrievi entity for the edit form.
     *
     * @return Entity to be edited.
     */
    Object getEntityForm();
}
