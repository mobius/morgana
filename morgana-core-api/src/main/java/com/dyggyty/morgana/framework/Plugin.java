package com.dyggyty.morgana.framework;

import com.dyggyty.morgana.framework.function.PluginFunction;

import java.util.List;

public interface Plugin {

    List<Class<? extends PluginFunction>> getPluginFunctions();

    /**
     * Return plugin name.
     *
     * @return Plugin name.
     */
    String getName();
}
