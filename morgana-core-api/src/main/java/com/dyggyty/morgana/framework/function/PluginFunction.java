package com.dyggyty.morgana.framework.function;

import com.dyggyty.morgana.framework.MorganaRequest;
import com.dyggyty.morgana.framework.Plugin;

public interface PluginFunction {

    /**
     * Property name that contains navigation path to the function.
     */
    static final String NAVIGATION = "navigation";

    FunctionResponse execute(MorganaRequest functionRequest) throws Exception;

    String getFunctionName();

    /**
     * Indicates that action will be shown on entity list by default.
     *
     * @param entityClass Type of the entity.
     * @return <b>TRUE</b> if action is applied on the entity lis and <b>FALSE</b> otherwise.
     */
    <T> boolean isDefaultOnEntityList(Class<T> entityClass);

    /**
     * Indicates that action will be shown on entity form by default.
     *
     * @param entityClass Type of the entity.
     * @return <b>TRUE</b> if action is applied on the entity lis and <b>FALSE</b> otherwise.
     */
    <T> boolean isDefaultOnEntityForm(Class<T> entityClass);

    /**
     * Set plugin instance to the function.
     *
     * @param plugin Plugin.
     */
    void setPlugin(Plugin plugin);
}
