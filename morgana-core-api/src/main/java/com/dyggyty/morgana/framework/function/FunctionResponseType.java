package com.dyggyty.morgana.framework.function;

/**
 * Type of the response.
 */
public enum FunctionResponseType {

    /**
     * Return to previous action.
     */
    BACK,

    /**
     * Forward to specific action with original attributes.
     */
    FORWARD,

    /**
     * Forward to specific action with modified attributes.
     */
    NEXT,

    /**
     * Open specified page.
     */
    PAGE
}
