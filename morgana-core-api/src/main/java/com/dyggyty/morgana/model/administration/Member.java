package com.dyggyty.morgana.model.administration;

import com.dyggyty.morgana.configuration.EntityDescriptor;
import com.dyggyty.morgana.configuration.FieldDescriptor;
import com.dyggyty.morgana.configuration.MenuItem;
import com.dyggyty.morgana.entity.FieldTemplate;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "member")
@MenuItem("menu.users")
@EntityDescriptor("menu.user")
public class Member implements Serializable {

    @Id
    @Column(length = 150)
    @FieldDescriptor("label.user.login")
    private String login;

    @Column(nullable = false, length = 256)
    @FieldDescriptor(value = "label.password", template = FieldTemplate.PASSWORD, hideOnTable = true)
    private String password;

    @Column(length = 150, nullable = false)
    @FieldDescriptor(value = "label.email", template = FieldTemplate.EMAIL)
    private String email;

    @Column(name = "first_name", length = 255)
    private String firstName;

    @Column(name = "last_name", length = 255)
    private String lastName;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Member member = (Member) o;

        return new EqualsBuilder()
                .append(login, member.login)
                .append(password, member.password)
                .append(email, member.email)
                .append(firstName, member.firstName)
                .append(lastName, member.lastName)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(login)
                .append(password)
                .append(email)
                .append(firstName)
                .append(lastName)
                .toHashCode();
    }

    @Override
    public String toString() {
        return getLogin();
    }
}
