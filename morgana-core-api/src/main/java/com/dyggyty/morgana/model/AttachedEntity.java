package com.dyggyty.morgana.model;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Generic class for the entity extender.
 */
@MappedSuperclass
public abstract class AttachedEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String mappedEntityId;

    private String mappedEntityType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMappedEntityId() {
        return mappedEntityId;
    }

    public void setMappedEntityId(String mappedEntity) {
        this.mappedEntityId = mappedEntity;
    }

    public String getMappedEntityType() {
        return mappedEntityType;
    }

    public void setMappedEntityType(String mappedEntityType) {
        this.mappedEntityType = mappedEntityType;
    }
}
