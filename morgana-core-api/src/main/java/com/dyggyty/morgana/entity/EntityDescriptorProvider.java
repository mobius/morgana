package com.dyggyty.morgana.entity;

import com.dyggyty.morgana.dto.EntityDescriptorDto;

import java.util.Set;

/**
 * Provide information about registered entities.
 */
public interface EntityDescriptorProvider {

    /**
     * Provide registered entity descriptors.
     *
     * @return Set of entity descriptors.
     */
    Set<EntityDescriptorDto> getEntityDescriptors();

    /**
     * Provide entity detailed information.
     *
     * @param entityName Name of the entity.
     * @return EntityDescriptor
     */
    EntityDescriptorDto getEntityDescriptor(String entityName);

    /**
     * Provide entity detailed information.
     *
     * @param entityClass Name of the entity.
     * @return EntityDescriptor
     */
    <T> EntityDescriptorDto<T> getEntityDescriptor(Class<T> entityClass);
}
