package com.dyggyty.morgana.entity;

/**
 * Contains list of field templates.
 */
public enum FieldTemplate {

    /**
     * Detect template automatically.
     */
    AUTO(null),
    ID("id"),
    STRING("string"),
    NUMBER("number"),
    DATE("date"),
    PASSWORD("password"),
    EMAIL("email"),
    REFERENCE("reference");

    private String name;

    private FieldTemplate(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
