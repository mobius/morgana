package com.dyggyty.morgana.entity;

import com.dyggyty.morgana.dto.AttachedEntityDescriptorDto;
import com.dyggyty.morgana.model.AttachedEntity;

import java.util.List;

/**
 * Extends entity fields.
 */
public interface EntityExtender<T extends AttachedEntity> {

    /**
     * Return attached entity descriptor for the entity class.
     *
     * @param entityClass Entity class.
     * @return Attached entity descriptor.
     */
    AttachedEntityDescriptorDto<T> getAttacheEntityDescriptor(Class<?> entityClass);

    /**
     * Return list of attached entities
     *
     * @param entityId   Identifier of the main entity.
     * @param entityName Name of the main entity.
     * @return List of the attached entities.
     */
    List<T> getAttachedEntities(String entityId, String entityName);

    /**
     * Modify entity extensions in main entity creation case.
     *
     * @param mainEntity       Main entity has been created.
     * @param attachedEntities List of attached entities.
     * @return List of modified attached entities.
     */
    List<T> onEntityCreate(Object mainEntity, List<T> attachedEntities);

    /**
     * Modify entity extensions in main entity updating case.
     *
     * @param mainEntityBefore Main entity before update.
     * @param mainEntityAfter  Main entity after update.
     * @param attachedEntities List of attached entities.
     * @return List of modified attached entities.
     */
    List<T> onEntityUpdate(Object mainEntityBefore, Object mainEntityAfter, List<T> attachedEntities);
}
