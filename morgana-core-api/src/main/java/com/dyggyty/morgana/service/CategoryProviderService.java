package com.dyggyty.morgana.service;

import com.dyggyty.morgana.dto.CategoryDto;

import java.util.List;

/**
 * Aggregates information about categories.
 */
public interface CategoryProviderService {

    /**
     * Provides category tree.
     *
     * @return Root category set.
     */
    List<CategoryDto> getCategories();
}
