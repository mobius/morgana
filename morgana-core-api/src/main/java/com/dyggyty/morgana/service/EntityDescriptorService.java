package com.dyggyty.morgana.service;

import com.dyggyty.morgana.dto.EntityDescriptorDto;
import com.dyggyty.morgana.persistence.EntityCrudRepository;

import java.io.Serializable;

/**
 * Provide entity information across registered EntityDescriptorProviders
 */
public interface EntityDescriptorService {

    /**
     * Provides information about entity;
     *
     * @param entityClass Class of the registered entity.
     * @return Entity descriptor.
     */
    EntityDescriptorDto getEntityDescriptor(Class entityClass);

    /**
     * Provides information about entity;
     *
     * @param entityName Name of the registered entity.
     * @return Entity descriptor.
     */
    EntityDescriptorDto getEntityDescriptor(String entityName);

    /**
     * Get CRUD repository implementation for the entity class.
     *
     * @param entityName Name of the registered entity.
     * @param <KEY>      Entity identifier type.
     * @return Entity CRUD repository.
     */
    <KEY extends Serializable> EntityCrudRepository<?, KEY> getCrudRepository(String entityName);

    /**
     * Creates Entity CRUD repository by class.
     *
     * @param repositoryClass CRUD repository class.
     * @param entityName      Name of the registered entity.
     * @param <KEY>           Entity identifier type.
     * @param <T>             Type of the CRUD repository
     * @return CRUD repository instance.
     */
    <KEY extends Serializable, T extends EntityCrudRepository<?, KEY>> T getCrudRepository(Class<T> repositoryClass,
                                                                                           String entityName);
}
