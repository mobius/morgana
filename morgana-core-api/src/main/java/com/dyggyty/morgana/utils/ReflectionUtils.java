package com.dyggyty.morgana.utils;

import com.dyggyty.morgana.configuration.MenuItem;
import com.dyggyty.morgana.dto.AbstractDocumentFieldDto;
import com.dyggyty.morgana.dto.CategoryDto;
import com.dyggyty.morgana.dto.CollectionFieldDto;
import com.dyggyty.morgana.dto.DocumentFieldDto;
import com.dyggyty.morgana.dto.EntityDescriptorDto;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.type.SingleColumnType;
import org.hibernate.type.StringRepresentableType;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

public class ReflectionUtils {

    private static final Log LOGGER = LogFactory.getLog(ReflectionUtils.class);
    private static final BeanUtilsBean BEAN_UTILS_BEAN = BeanUtilsBean.getInstance();

    private ReflectionUtils() {
        //private constructor
    }

    public static MenuItem getParentMenuItem(Package currentPackage) {

        MenuItem menuItem = currentPackage.getDeclaredAnnotation(MenuItem.class);

        if (menuItem == null) {
            String packageName = currentPackage.getName();
            final ClassLoader classLoader = ReflectionUtils.class.getClassLoader();

            while (packageName.contains(".") && menuItem == null) {
                packageName = packageName.substring(0, packageName.lastIndexOf("."));
                try {
                    classLoader.loadClass(packageName + ".package-info");
                    Package controllerPackage = Package.getPackage(packageName);
                    menuItem = controllerPackage.getDeclaredAnnotation(MenuItem.class);
                } catch (ClassNotFoundException e) {
                    //empty catch here
                }
            }
        }

        return menuItem;
    }

    public static CategoryDto getParentCategoryItem(Package currentPackage) {
        MenuItem menuItem = getParentMenuItem(currentPackage);

        if (menuItem != null) {
            return EntityUtils.createCategoryDto(menuItem);
        }

        return null;
    }

    /**
     * Get entity identifier value.
     *
     * @param entityDescriptorDto Entity descriptor.
     * @param entity              Entity instance.
     * @return Identifier value.
     */
    public static <T extends Serializable> T getIdValue(EntityDescriptorDto entityDescriptorDto, Object entity) {
        List<DocumentFieldDto> documentFieldDtos = entityDescriptorDto.getDocumentFields();

        for (DocumentFieldDto documentFieldDto : documentFieldDtos) {
            if (documentFieldDto.isId()) {
                return getPoorValue(documentFieldDto, entity);
            }
        }

        return null;
    }


    public static <T extends Serializable> T getPoorValue(Member member, Object entity) {

        Object value = null;
        try {
            if (member instanceof Field) {
                value = BEAN_UTILS_BEAN.getPropertyUtils().getProperty(entity, member.getName());
            } else if (member instanceof Method) {
                value = ((Method) member).invoke(entity);
            }
        } catch (InvocationTargetException | IllegalAccessException | NoSuchMethodException e) {
            LOGGER.error("Can't get value for the field '" + member.getName());
        }

        if (value == null) {
            return null;
        }

        return (T) value;
    }

    public static <T extends Serializable> T getPoorValue(AbstractDocumentFieldDto documentFieldDto, Object entity) {
        return getPoorValue(documentFieldDto.getMember(), entity);
    }

    /**
     * Set value to the entity.
     *
     * @param documentFieldDto Document field descriptor DTO.
     * @param entity           Entity instance.
     * @param value            Value to set.
     * @param <T>              Type of the value.
     */
    public static <T extends Serializable> void setPoorValue(DocumentFieldDto documentFieldDto, Object entity, Object value) {

        Member member = documentFieldDto.getMember();
        setPoorValue(member, entity, value);
    }

    /**
     * Set value to the entity.
     *
     * @param member Document field member.
     * @param entity Entity instance.
     * @param value  Value to set.
     * @param <T>    Type of the value.
     */
    public static <T extends Serializable> void setPoorValue(Member member, Object entity, Object value) {

        String propertyName = member.getName();
        if (member instanceof Method) {
            propertyName = Character.toLowerCase(propertyName.charAt(3)) + propertyName.substring(4);
        }

        setPoorValue(propertyName, entity, value);
    }

    public static <T extends Serializable> void setPoorValue(String propertyName, Object entity, Object value) {

        try {
            BEAN_UTILS_BEAN.getPropertyUtils().setProperty(entity, propertyName, value);
        } catch (InvocationTargetException | IllegalAccessException | NoSuchMethodException e) {
            LOGGER.error("Can't get value for the field '" + propertyName);
        }
    }

    /**
     * Looking for tabbed document field descriptor,
     *
     * @param entityDescriptorDto Entity descriptor.
     * @param fieldName           Tab field name.
     * @return Document field descriptor for the specified tab name.
     */
    public static CollectionFieldDto getDocumentTabFieldDto(EntityDescriptorDto entityDescriptorDto, String fieldName) {

        List<CollectionFieldDto> documentFields = entityDescriptorDto.getDocumentFieldTabs();

        CollectionFieldDto currentField = null;
        for (CollectionFieldDto fieldTabDto : documentFields) {
            if (fieldName.equals(fieldTabDto.getName())) {
                currentField = fieldTabDto;
                break;
            }
        }

        return currentField;
    }

    /**
     * Return field value from string representation.
     *
     * @param fieldType Hibernate java type.
     * @param value     String representation of field value.
     * @return Field value.
     */
    public static Object getFieldValue(Type fieldType, String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }

        if (fieldType instanceof SingleColumnType) {
            return ((SingleColumnType) fieldType).fromStringValue(value);
        } else if (fieldType instanceof StringRepresentableType) {
            return ((StringRepresentableType) fieldType).fromStringValue(value);
        }

        throw new IllegalArgumentException("Can't convert '" + value + "' to the " + fieldType.getName() + " type");
    }

    /**
     * Creates object instance from the entity information and String data.
     *
     * @param classMetadata Class metadata details.
     * @param entity        Entity string values map.
     * @param <ENTITY>      Entity to restore.
     * @return Entity instance.
     * @throws ReflectiveOperationException Throws in case runtime errors.
     */
    public static <ENTITY> ENTITY createObject(ClassMetadata classMetadata, Map<String, String> entity)
            throws ReflectiveOperationException {

        ENTITY entityInstance = (ENTITY) classMetadata.getMappedClass().newInstance();
        PropertyUtilsBean propertyUtilsBean = BEAN_UTILS_BEAN.getPropertyUtils();

        //Copying ID value.
        String idName = classMetadata.getIdentifierPropertyName();
        Type idType = classMetadata.getIdentifierType();
        Object idValue = ReflectionUtils.getFieldValue(idType, entity.get(idName));
        propertyUtilsBean.setProperty(entityInstance, idName, idValue);

        String[] propertyNames = classMetadata.getPropertyNames();
        Type[] propertyTypes = classMetadata.getPropertyTypes();

        //Copying property values.
        for (int i = 0; i < propertyTypes.length; i++) {
            Type propertyType = propertyTypes[i];
            String propertyName = propertyNames[i];

            Object propertyValue = ReflectionUtils.getFieldValue(propertyType, entity.get(propertyName));
            propertyUtilsBean.setProperty(entityInstance, propertyName, propertyValue);
        }

        return entityInstance;
    }
}
