package com.dyggyty.morgana.utils;

import com.dyggyty.morgana.configuration.MenuItem;
import com.dyggyty.morgana.dto.CategoryDto;
import com.dyggyty.morgana.dto.EntityDescriptorDto;
import com.dyggyty.morgana.entity.EntityDescriptorProvider;
import com.dyggyty.morgana.entity.FieldTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

/**
 * Provides utility classes to work with entity
 */
public class EntityUtils {

    private EntityUtils() {
        //private constructor
    }

    /**
     * Create new category class for menu item.
     *
     * @param menuItem Menu item will be used to create Category.
     * @return Created category DTO.
     */
    public static CategoryDto createCategoryDto(MenuItem menuItem) {

        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setName(menuItem.value());
        categoryDto.setOrder(menuItem.order());
        categoryDto.setChildren(new ArrayList<>());

        return categoryDto;
    }

    /**
     * Return field template from class.
     *
     * @param fieldClass Field class.
     * @return Display template.
     */
    public static FieldTemplate getFieldTemplate(Class fieldClass,
                                                 EntityDescriptorProvider entityDescriptorProvider) {

        if (Number.class.isAssignableFrom(fieldClass)) {
            return FieldTemplate.NUMBER;
        } else if (Date.class.equals(fieldClass)) {
            return FieldTemplate.DATE;
        } else if (entityDescriptorProvider != null && entityDescriptorProvider.getEntityDescriptor(fieldClass) != null) {
            return FieldTemplate.REFERENCE;
        }

        return FieldTemplate.STRING;
    }

    /**
     * Finds entity descriptor from class.
     *
     * @param entityClass               Field class.
     * @param entityDescriptorProviders Set of Descriptor providers.
     * @return Entity descriptor.
     */
    public static EntityDescriptorDto getEntityDescriptor(Class entityClass,
                                                          Set<EntityDescriptorProvider> entityDescriptorProviders) {

        EntityDescriptorDto entityDescriptorDto = null;
        for (EntityDescriptorProvider entityDescriptorProvider : entityDescriptorProviders) {
            entityDescriptorDto = entityDescriptorProvider.getEntityDescriptor(entityClass);
            if (entityDescriptorDto != null) {
                break;
            }
        }

        return entityDescriptorDto;
    }
}
