package com.dyggyty.morgana.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Contains utility methods to work with collections.
 */
public class CollectionUtils {

    private static final Log LOGGER = LogFactory.getLog(CollectionUtils.class);

    private CollectionUtils() {
        //empty constructor
    }

    /**
     * Retrieve from map or create new object and put in into the map with thread save method.
     *
     * @param map      Map will be used.
     * @param key      Key to retrieve or put object into the map.
     * @param function Function will be called to create the object if it is not <b>NULL</b> and not in the map.
     * @param <RESULT> Result that will be returned by function and placed into the map.
     * @param <KEY>    Key will be used to get/put value from the map.
     * @return Created or cached object.
     */
    public static <RESULT, KEY> RESULT getOrCreateSynchronously(final Map<KEY, RESULT> map, KEY key,
                                                                Callable<RESULT> function) {
        RESULT targetObject = map.get(key);

        if (targetObject == null) {
            synchronized (map) {
                targetObject = map.get(key);

                if (targetObject == null) {
                    try {
                        targetObject = function.call();
                    } catch (Exception e) {
                        LOGGER.error("Can't execute operation", e);
                    }

                    if (targetObject != null) {
                        map.put(key, targetObject);
                    }
                }
            }
        }

        return targetObject;
    }

    /**
     * @see Arrays#asList(Object[])
     */
    @SafeVarargs
    public static <T> List<T> listOfElements(T... elements) {
        return Arrays.asList(elements);
    }

    /**
     * Add value to map of lists.<br/>
     * If there is no existing list then creates new one.
     *
     * @param map   Map of lists.
     * @param key   Key of elements.
     * @param value Value to be added into the map.
     * @param <T>   Type of the value
     * @return List passed into the argument.
     */
    public static <T> Map<String, List<T>> addToMapOfLists(Map<String, List<T>> map, String key, T value) {
        List<T> elements = map.get(key);
        if (elements == null) {
            elements = new ArrayList<>();
            map.put(key, elements);
        }

        elements.add(value);

        return map;
    }
}
