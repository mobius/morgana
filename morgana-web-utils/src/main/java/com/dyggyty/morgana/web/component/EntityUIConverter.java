package com.dyggyty.morgana.web.component;

import com.dyggyty.morgana.dto.EntityDescriptorDto;
import com.dyggyty.morgana.persistence.EntityCrudRepository;
import com.dyggyty.morgana.service.EntityDescriptorService;
import com.dyggyty.morgana.utils.ReflectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.io.Serializable;

/**
 * Converts entity for UI and back.
 */
@Component(EntityUIConverter.ENTITY_UI_CONVERTER)
public class EntityUIConverter implements Converter {

    public static final String ENTITY_UI_CONVERTER = "entityUIConverter";

    @Autowired
    private EntityDescriptorService entityDescriptorService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        int splitterIndex = value.lastIndexOf(":");
        String entityType = value.substring(splitterIndex + 1);
        String id = value.substring(0, splitterIndex);
        EntityCrudRepository entityCrudRepository = entityDescriptorService.getCrudRepository(entityType);
        return entityCrudRepository.findOne(id);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        EntityDescriptorDto entityDescriptorDto = entityDescriptorService.getEntityDescriptor(value.getClass());
        Serializable idValue = ReflectionUtils.getIdValue(entityDescriptorDto, value);
        return idValue + ":" + entityDescriptorDto.getEntityName();
    }
}
