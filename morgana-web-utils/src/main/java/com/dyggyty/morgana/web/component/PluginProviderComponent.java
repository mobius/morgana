package com.dyggyty.morgana.web.component;

import com.dyggyty.morgana.framework.MorganaRequest;
import com.dyggyty.morgana.framework.Plugin;
import com.dyggyty.morgana.framework.PluginFunctionFactory;
import com.dyggyty.morgana.framework.function.EntityProviderFunctionResponse;
import com.dyggyty.morgana.framework.function.FunctionResponse;
import com.dyggyty.morgana.framework.function.FunctionResponseType;
import com.dyggyty.morgana.framework.function.PluginFunction;
import com.dyggyty.morgana.web.utils.HttpUtils;
import com.dyggyty.morgana.web.utils.InternalServerError;
import com.dyggyty.morgana.web.utils.ResourceNotFoundException;
import org.apache.commons.collections4.OrderedMapIterator;
import org.apache.commons.collections4.map.ListOrderedMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static com.dyggyty.morgana.framework.function.FunctionResponseType.PAGE;

/**
 * Provides plugin executions
 */
@ManagedBean(name = PluginProviderComponent.PLUGIN_PROVIDER)
@RequestScoped
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
@Component(PluginProviderComponent.PLUGIN_PROVIDER)
public class PluginProviderComponent implements Serializable {

    public static final String PLUGIN_PROVIDER = "pluginProvider";
    public static final String DEFAULT_PAGE = "default";

    public static final String FUNCTION_CALL_COMMAND = "#{" + PLUGIN_PROVIDER + ".executeFunction}";

    private final Log LOGGER = LogFactory.getLog(this.getClass());

    @Autowired
    private PluginFunctionFactory pluginFunctionFactory;

    private String pluginName = DEFAULT_PAGE;
    private FunctionResponse functionResponse;
    private Object entityForm;
    private String functionPage = DEFAULT_PAGE;
    private Plugin plugin;

    public FunctionResponse executeFunctionInCurrentContext(String functionName) throws Exception {
        PluginFunction pluginFunction = pluginFunctionFactory.getPluginFunction(plugin, functionName);
        MorganaRequest functionRequest = HttpUtils.createMorganaRequest(entityForm);
        return pluginFunction.execute(functionRequest);
    }

    public void executeFunction() {

        MorganaRequest functionRequest = HttpUtils.createMorganaRequest(entityForm);

        functionResponse = null;
        entityForm = null;

        Map<String, String> params =
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

        pluginName = params.get(HttpUtils.PLUGIN_NAME);
        plugin = pluginFunctionFactory.getPlugin(pluginName);
        PluginFunction pluginFunction = pluginFunctionFactory.getPluginFunction(plugin, params.get(HttpUtils.FUNCTION_NAME));

        FunctionResponseType functionResponseType = null;

        ListOrderedMap<Class<? extends PluginFunction>, MorganaRequest> navigation = HttpUtils.getNavigation();

        try {
            while (!PAGE.equals(functionResponseType)) {

                Class<? extends PluginFunction> pluginFunctionClass =
                        (Class<? extends PluginFunction>) AopUtils.getTargetClass(pluginFunction);

                if (navigation.containsKey(pluginFunctionClass)) {
                    OrderedMapIterator<Class<? extends PluginFunction>, MorganaRequest> iterator =
                            navigation.mapIterator();

                    while (iterator.hasNext()) {
                        Class<? extends PluginFunction> savedFunctionClass = iterator.next();
                        if (savedFunctionClass.equals(pluginFunctionClass)) {
                            iterator.remove();
                            while (iterator.hasNext()) {
                                iterator.next();
                                iterator.remove();
                            }
                        }
                    }
                }

                //todo detect loop
                functionResponse = pluginFunction.execute(functionRequest);

                if (functionResponse instanceof EntityProviderFunctionResponse) {
                    entityForm = ((EntityProviderFunctionResponse) functionResponse).getEntityForm();
                }

                if (functionResponse == null) {
                    throw new InternalServerError();
                }

                functionPage = functionResponse.getView();
                functionResponseType = functionResponse.getResponseType();

                switch (functionResponseType) {
                    case BACK: {
                        if (navigation.size() < 2) {
                            throw new ResourceNotFoundException();
                        }

                        Class<? extends PluginFunction> functionClass = navigation.get(navigation.size() - 2);
                        pluginFunction = pluginFunctionFactory.getPluginFunction(functionClass).getValue();
                        functionRequest = navigation.get(functionClass);
                        break;
                    }

                    case FORWARD: {
                        Class<? extends PluginFunction> functionClass = functionResponse.getPluginFunctionClass();
                        pluginFunction = pluginFunctionFactory.getPluginFunction(functionClass).getValue();
                        break;
                    }

                    case NEXT: {
                        Class<? extends PluginFunction> functionClass = functionResponse.getPluginFunctionClass();
                        pluginFunction = pluginFunctionFactory.getPluginFunction(functionClass).getValue();

                        Map<String, String[]> modifiedArguments = new HashMap<>();
                        modifiedArguments.putAll(functionRequest.getParameters());
                        modifiedArguments.putAll(functionResponse.getRequestArguments());

                        functionRequest = new MorganaRequest(functionRequest.getSession().getSessionAttributes(),
                                modifiedArguments);
                    }

                    default: {
                        navigation.put(pluginFunctionClass, functionRequest);
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.error("Error during function execution", ex);
        }
    }

    public String getPluginName() {
        return pluginName;
    }

    public FunctionResponse getFunctionResponse() {
        return functionResponse;
    }

    public String getFunctionPage() {
        return functionPage;
    }

    public Object getEntityForm() {
        return entityForm;
    }

    public Plugin getPlugin() {
        return plugin;
    }
}
