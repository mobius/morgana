package com.dyggyty.morgana.web.utils;

import com.dyggyty.morgana.framework.MorganaRequest;
import com.dyggyty.morgana.framework.function.PluginFunction;
import org.apache.commons.collections4.map.ListOrderedMap;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Provide WEB utils
 */
public class HttpUtils {

    public static final String PLUGIN_NAME = "pluginName";
    public static final String FUNCTION_NAME = "functionName";

    private HttpUtils() {
        //private constructor.
    }

    /**
     * Create Morgana request from Http request.
     *
     * @param httpServletRequest Http request.
     * @return Morgana request instance.
     */
    public static MorganaRequest createMorganaRequest(HttpServletRequest httpServletRequest) {

        final Map<String, String[]> requestParameters = httpServletRequest.getParameterMap();
        HttpSession session = httpServletRequest.getSession();
        Map<String, Object> sessionAttributes = getSessionAttributes(session);

        return new MorganaRequest(sessionAttributes, new HashMap<>(requestParameters));
    }

    /**
     * Create Morgana request from JSF Context.
     *
     * @return Morgana request instance.
     */
    public static MorganaRequest createMorganaRequest(Object entityForm) {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        final Map<String, String[]> jsfParameters = facesContext.getExternalContext().getRequestParameterValuesMap();
        final Map<String, String[]> requestParameters = new HashMap<>();
        jsfParameters.forEach((paramName, paramValues) -> {
            if (!(paramName.startsWith("j_") || paramName.startsWith("javax."))) {
                requestParameters.put(paramName, paramValues);
            }
        });
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
        Map<String, Object> sessionAttributes = getSessionAttributes(session);

        return new MorganaRequest(sessionAttributes, requestParameters, entityForm);
    }

    /**
     * Retrieve navigation from HTTP request.
     *
     * @param request HTTP request
     * @return Navigation object.
     */
    public static ListOrderedMap<Class<? extends PluginFunction>, MorganaRequest> getNavigation(
            HttpServletRequest request) {
        HttpSession httpSession = request.getSession();

        return getNavigation(httpSession);
    }

    public static ListOrderedMap<Class<? extends PluginFunction>, MorganaRequest> getNavigation(
            HttpSession httpSession) {
        Object navigation = httpSession.getAttribute(PluginFunction.NAVIGATION);

        if (navigation == null) {
            navigation = new ListOrderedMap<>();
            httpSession.setAttribute(PluginFunction.NAVIGATION, navigation);
        }

        //noinspection unchecked
        return (ListOrderedMap<Class<? extends PluginFunction>, MorganaRequest>) navigation;
    }

    /**
     * Return current browser locase for JSF.
     *
     * @return Browser locale.
     */
    public static Locale getBrowserLocale() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
    }

    public static ListOrderedMap<Class<? extends PluginFunction>, MorganaRequest> getNavigation() {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);

        return getNavigation(session);
    }

    private static Map<String, Object> getSessionAttributes(HttpSession session) {
        Map<String, Object> sessionAttributes = new HashMap<>();
        Enumeration<String> attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String attributeName = attributeNames.nextElement();
            sessionAttributes.put(attributeName, session.getAttribute(attributeName));
        }
        return sessionAttributes;
    }
}
