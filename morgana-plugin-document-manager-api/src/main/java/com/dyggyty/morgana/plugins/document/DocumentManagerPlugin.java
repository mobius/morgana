package com.dyggyty.morgana.plugins.document;

import com.dyggyty.morgana.framework.CategoryProvider;
import com.dyggyty.morgana.framework.Plugin;
import com.dyggyty.morgana.plugins.document.ui.FieldDescriptorComposite;
import com.dyggyty.morgana.plugins.document.ui.SelectOneMenuEntityComposite;

import java.util.Collection;
import java.util.List;

/**
 * Provides document plugin functionality
 */
public interface DocumentManagerPlugin extends Plugin, CategoryProvider {

    /**
     * Return list of entities for the type.
     *
     * @param entityType Registered type of the entity.
     * @return List of entity composites for the select element.
     */
    public List<SelectOneMenuEntityComposite> getEntityCompositeForType(String entityType);

    /**
     * Return list of linked entities for the specified object.
     *
     * @param entity    Entity database object.
     * @param fieldName Name of the field.
     * @return List of linked entities.
     */
    public Collection getAttachedObjects(Object entity, String fieldName);

    public List<FieldDescriptorComposite> getFieldDescriptorCompositesForList(String entityType);
}
