package com.dyggyty.morgana.plugins.document.ui;

public class TabbedFieldDescriptorComposite extends AbstractFieldDescriptorComposite {

    private String fieldType;

    public TabbedFieldDescriptorComposite(String title, String fieldName, String fieldType) {
        super(title, fieldName);
        this.fieldType = fieldType;
    }

    public String getFieldType() {
        return fieldType;
    }
}
