package com.dyggyty.morgana.plugins.document.ui;

public abstract class AbstractFieldDescriptorComposite {

    private String title;
    private String fieldName;

    public AbstractFieldDescriptorComposite(String title, String fieldName) {
        this.title = title;
        this.fieldName = fieldName;
    }

    public String getTitle() {
        return title;
    }

    public String getFieldName() {
        return fieldName;
    }
}
