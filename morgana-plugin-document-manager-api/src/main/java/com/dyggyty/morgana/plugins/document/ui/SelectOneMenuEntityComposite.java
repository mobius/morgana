package com.dyggyty.morgana.plugins.document.ui;

public class SelectOneMenuEntityComposite {

    private Object entity;
    private String entityName;

    public SelectOneMenuEntityComposite(Object entity, String entityName) {
        this.entity = entity;
        this.entityName = entityName;
    }

    public Object getEntity() {
        return entity;
    }

    public String getEntityName() {
        return entityName;
    }
}
