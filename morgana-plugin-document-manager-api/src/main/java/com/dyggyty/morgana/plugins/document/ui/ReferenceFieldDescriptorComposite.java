package com.dyggyty.morgana.plugins.document.ui;

import com.dyggyty.morgana.entity.FieldTemplate;

public class ReferenceFieldDescriptorComposite extends FieldDescriptorComposite {

    private String fieldType;

    public ReferenceFieldDescriptorComposite(String title, String fieldName, String fieldType) {
        super(title, fieldName, FieldTemplate.REFERENCE.getName(), false);
        this.fieldType = fieldType;
    }

    public String getFieldType() {
        return fieldType;
    }
}
