package com.dyggyty.morgana.plugins.document.ui;

public class FieldDescriptorComposite extends AbstractFieldDescriptorComposite {

    private String template;
    private boolean id = false;

    public FieldDescriptorComposite(String title, String fieldName, String template, boolean idFlag) {
        super(title, fieldName);
        this.template = template;
        this.id = idFlag;
    }

    public String getTemplate() {
        return template;
    }

    public boolean isId() {
        return id;
    }
}
