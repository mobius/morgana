package com.dyggyty.morgana.widget;

import com.dyggyty.morgana.configuration.WidgetConfiguration;
import com.dyggyty.morgana.dto.CategoryDto;
import com.dyggyty.morgana.framework.Widget;
import com.dyggyty.morgana.service.CategoryProviderService;
import com.dyggyty.morgana.web.component.PluginProviderComponent;
import com.dyggyty.morgana.web.utils.HttpUtils;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@ManagedBean(name = VerticalMenuWidget.WIDGET_NAME)
@RequestScoped
@WidgetConfiguration(value = VerticalMenuWidget.WIDGET_NAME, position = WidgetConfiguration.Position.LEFT_PANE)
public class VerticalMenuWidget implements Widget {

    public static final String WIDGET_NAME = "verticalmenu";

    @Autowired
    private MessageSource messages;

    @Autowired
    private CategoryProviderService categoryProviderService;

    public MenuModel getMenuModel() {

        MenuModel menuModel = new DefaultMenuModel();

        List<CategoryDto> categories = categoryProviderService.getCategories();
        Locale browserLocale = HttpUtils.getBrowserLocale();

        for (CategoryDto categoryDto : categories) {

            String categoryTitle = categoryDto.getName();
            DefaultSubMenu submenu =
                    new DefaultSubMenu(
                            messages.getMessage(categoryTitle, new Object[]{}, categoryTitle, browserLocale));
            submenu.setExpanded(true);

            for (CategoryDto child : categoryDto.getChildren()) {

                String childMenuTitle = child.getName();
                String menuItemTitle =
                        messages.getMessage(childMenuTitle, new Object[]{}, childMenuTitle, browserLocale);
                DefaultMenuItem childMenu = new DefaultMenuItem(menuItemTitle);
                childMenu.setTitle(menuItemTitle);
                childMenu.setAjax(false);

                childMenu.setParam(HttpUtils.PLUGIN_NAME, child.getPluginName());
                childMenu.setParam(HttpUtils.FUNCTION_NAME, child.getPluginFunctionName());

                Map<String, String> arguments = child.getArguments();
                if (arguments != null) {
                    arguments.forEach(childMenu::setParam);
                }

                childMenu.setCommand(PluginProviderComponent.FUNCTION_CALL_COMMAND);

                submenu.addElement(childMenu);
            }

            menuModel.addElement(submenu);
        }

        if (menuModel.getElements().size() > 0) {
            menuModel.generateUniqueIds();
        }

        return menuModel;
    }
}
