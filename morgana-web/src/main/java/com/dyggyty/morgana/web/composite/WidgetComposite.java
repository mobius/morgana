package com.dyggyty.morgana.web.composite;

import com.dyggyty.morgana.configuration.WidgetConfiguration;

public class WidgetComposite implements Comparable<WidgetComposite> {

    private String widgetName;

    private String applyToExpr;

    private String position;

    private int order;

    public WidgetComposite(String widgetName, String applyToExpr, String position) {
        this(widgetName, applyToExpr, position, Integer.MAX_VALUE);
    }

    public WidgetComposite(String widgetName, String applyToExpr, String position, int order) {
        this.widgetName = widgetName;
        this.applyToExpr = applyToExpr;
        this.position = position;
        this.order = order;
    }

    public String getWidgetName() {
        return widgetName;
    }

    public String getApplyToExpr() {
        return applyToExpr;
    }

    public String getPosition() {
        return position;
    }

    public int getOrder() {
        return order;
    }

    @Override
    public int compareTo(WidgetComposite o) {
        return Integer.compare(order, o.order);
    }
}
