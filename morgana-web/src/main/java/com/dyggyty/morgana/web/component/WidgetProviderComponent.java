package com.dyggyty.morgana.web.component;

import com.dyggyty.morgana.configuration.WidgetConfiguration;
import com.dyggyty.morgana.utils.CollectionUtils;
import com.dyggyty.morgana.web.composite.WidgetComposite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Provides information about configured widgets.
 */
@ManagedBean(name = WidgetProviderComponent.WIDGETS_PROVIDER)
@Component(WidgetProviderComponent.WIDGETS_PROVIDER)
public class WidgetProviderComponent {
    public static final String WIDGETS_PROVIDER = "widgetsProvider";

    private final Map<String, List<WidgetComposite>> widgets = new HashMap<>();

    @Autowired
    private ApplicationContext applicationContext;

    @PostConstruct
    public void init() {
        Map<String, Object> widgetMap = applicationContext.getBeansWithAnnotation(WidgetConfiguration.class);

        widgetMap.values().forEach(widget -> {
            WidgetConfiguration widgetConfiguration = widget.getClass().getDeclaredAnnotation(WidgetConfiguration.class);

            String widgetName = widgetConfiguration.value();
            String position = widgetConfiguration.position().getValue();
            String applyToExpr = widgetConfiguration.applyToExpr();
            int order = widgetConfiguration.order();

            WidgetComposite widgetComposite = new WidgetComposite(widgetName, applyToExpr, position, order);

            CollectionUtils.addToMapOfLists(widgets, position, widgetComposite);
        });

        widgets.forEach((position, widgets) -> Collections.sort(widgets));
    }

    public List<WidgetComposite> getWidgets(String position) {
        List<WidgetComposite> widgets = this.widgets.get(position);
        return widgets == null ? Collections.EMPTY_LIST : widgets;
    }

    public boolean hasWidgets(String position) {
        List<WidgetComposite> widgets = this.widgets.get(position);
        return widgets != null && widgets.size() > 0;
    }
}
