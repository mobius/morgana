package com.dyggyty.morgana.web.controller;

import com.dyggyty.morgana.configuration.MenuItem;
import com.dyggyty.morgana.framework.MorganaRequest;
import com.dyggyty.morgana.framework.function.PluginFunction;
import com.dyggyty.morgana.web.utils.HttpUtils;
import org.apache.commons.collections4.map.ListOrderedMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class HomePageController {

    @MenuItem(value = "menu.home", order = 0)
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView startPage(HttpServletRequest request) {

        //Clearing breadcrumbs in Home page case
        ListOrderedMap<Class<? extends PluginFunction>, MorganaRequest> navigation = HttpUtils.getNavigation(request);
        navigation.clear();

        return new ModelAndView("index");
    }
}
