package com.dyggyty.morgana.web;

import com.dyggyty.morgana.configuration.WidgetConfiguration;
import com.dyggyty.morgana.framework.MorganaRequest;
import com.dyggyty.morgana.framework.MorganaSession;
import com.dyggyty.morgana.framework.Widget;
import com.dyggyty.morgana.utils.CollectionUtils;
import com.dyggyty.morgana.web.utils.HttpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WidgetsBinderInterceptor extends HandlerInterceptorAdapter {

    public static final String REQUEST_ATTRIBUTE_WIDGETS = "widgets";
    private final List<Widget> widgets = new ArrayList<>();

    @Autowired
    private ApplicationContext applicationContext;

    @PostConstruct
    public void init() {
        Map<String, Widget> widgetMap = applicationContext.getBeansOfType(Widget.class);
        widgetMap.forEach(
                (name, widget) -> widgets.add(widget)
        );
    }

    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws ServletException {

        String requestPath = getRequestPath(httpServletRequest);

        final Map<String, List<Map.Entry<String, Widget>>> widgetsMap;
        if (httpServletRequest.getAttribute(REQUEST_ATTRIBUTE_WIDGETS) == null) {
            widgetsMap = new HashMap<>();
            httpServletRequest.setAttribute(REQUEST_ATTRIBUTE_WIDGETS, widgetsMap);
        } else {
            //noinspection unchecked
            widgetsMap = (Map) httpServletRequest.getAttribute(REQUEST_ATTRIBUTE_WIDGETS);
        }

        HttpSession session = httpServletRequest.getSession();

        MorganaRequest morganaRequest = HttpUtils.createMorganaRequest(httpServletRequest);

        widgets.forEach(widget -> {
            WidgetConfiguration widgetConfiguration = widget.getClass().getDeclaredAnnotation(WidgetConfiguration.class);
            String applyToExpr = widgetConfiguration.applyToExpr();

            if (requestPath.matches(applyToExpr)) {

                String widgetName = widgetConfiguration.value();

                String position = widgetConfiguration.position().getValue();

                CollectionUtils.addToMapOfLists(widgetsMap, position,
                        new AbstractMap.SimpleEntry<>(widgetName, widget));
            }
        });

        //Sorting widgets by order.
        widgetsMap.forEach((position, widgets) -> Collections.sort(widgets, (o1, o2) -> {
            WidgetConfiguration o1Configuration = o1.getValue().getClass().getDeclaredAnnotation(WidgetConfiguration.class);
            WidgetConfiguration o2Configuration = o2.getValue().getClass().getDeclaredAnnotation(WidgetConfiguration.class);
            return o1Configuration.order() - o2Configuration.order();
        }));

        MorganaSession morganaSession = morganaRequest.getSession();
        morganaSession.getSessionAttributes().forEach(session::setAttribute);
        morganaSession.getDeletedAttributes().forEach(session::removeAttribute);
    }

    private static String getRequestPath(HttpServletRequest request) {

        String appPath = request.getContextPath();
        String uri = request.getRequestURI();

        return uri.substring(appPath.length());
    }
}
