package com.dyggyty.morgana.web.controller.security.authentication;


/**
 * @author ilya.drabenia
 */
public class UserDetails {

    private String userName;

    public UserDetails() {

    }

    public UserDetails(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
