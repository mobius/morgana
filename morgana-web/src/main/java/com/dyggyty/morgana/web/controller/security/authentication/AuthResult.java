package com.dyggyty.morgana.web.controller.security.authentication;

/**
 * @author ilya.drabenia
 */
public class AuthResult {
    private int code;
    private String message;

    public AuthResult() {
    }

    public AuthResult(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
