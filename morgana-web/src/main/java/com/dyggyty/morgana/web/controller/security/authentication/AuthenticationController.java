package com.dyggyty.morgana.web.controller.security.authentication;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;

/**
 * @author ilya.drabenia
 */
//@Controller
@RequestMapping("/api/authentication")
public class AuthenticationController {

    //@Resource(name = "authenticationManager")
    private AuthenticationManager authManager;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public AuthResult login(@RequestBody LoginRequest loginRequest, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(loginRequest.getLogin(),
                loginRequest.getPassword());

        request.getSession();
        token.setDetails(new WebAuthenticationDetails(request));
        Authentication authenticatedUser = authManager.authenticate(token);

        SecurityContextHolder.getContext().setAuthentication(authenticatedUser);

        return new AuthResult(200, "Authenticated");
    }

    @ResponseBody
    @ExceptionHandler(AuthenticationException.class)
    public AuthResult loginFailed() {
        // remove session
        SecurityContextHolder.getContext().setAuthentication(null);
        return new AuthResult(401, "Bad Credentials");
    }

    @ResponseBody
    @RequestMapping(value = "/logout", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AuthResult logout(HttpServletRequest request) {
        // remove session
        SecurityContextHolder.getContext().setAuthentication(null);
        request.getSession().invalidate();
        return new AuthResult(200, "Logged Out");
    }

    @ResponseBody
    @RequestMapping(value = "/user/current", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @RolesAllowed("IS_AUTHENTICATED_FULLY")
    public UserDetails provideUserDetails() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

/*
        UserDetailsContainer detailsContainer = (UserDetailsContainer) authentication.getPrincipal();

        return createUserDetails(detailsContainer);
*/
        return null;
    }

//    private UserDetails createUserDetails(UserDetailsContainer detailsContainer) {
//        return new UserDetails(
//                detailsContainer.getUsername()
//                );
//    }
}
