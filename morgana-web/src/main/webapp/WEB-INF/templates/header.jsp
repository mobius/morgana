<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="span-24 header">
    <spring:url value="/resources/images/logo_135.png" var="logo" htmlEscape="true"/>
    <img src="${logo}" height="135px" style="padding-top:10px;"/>
</div>