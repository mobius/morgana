<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Spring MVC - Tiles Integration tutorial</title>

    <spring:url value="/resources/select2/css/select2.min.css" var="select2_css"/>
    <link href="${select2_css}" rel="stylesheet" type="text/css"/>
    <spring:url value="/resources/select2/js/select2.min.js" var="select2_js"/>
    <script src="${select2_js}" type="text/javascript"></script>

    <spring:url value="/resources/css/screen.css" var="screen_css"/>
    <link rel="stylesheet" href="${screen_css}" type="text/css" media="screen, projection"/>

    <spring:url value="/resources/css/print.css" var="print_css"/>
    <link rel="stylesheet" href="${print_css}" type="text/css" media="print"/>

    <style>
        body {
            margin-top: 20px;
            margin-bottom: 20px;
            background-color: #DFDFDF;
        }
    </style>
</head>
<body>
<div class="container" style="border: #C1C1C1 solid 1px; border-radius:10px;">
    <!-- Header -->
    <tiles:insertAttribute name="header"/>
    <!-- Menu Page -->
    <c:if test="${!empty requestScope.widgets && !empty requestScope.widgets['left_pane']}">
        <div class="span-5  border" style="min-height:400px; background-color:#FCFCFC;">
            <c:forEach var="widget" items="${requestScope.widgets['left_pane']}">
                <tiles:insertDefinition name="${widget.key}"/>
            </c:forEach>
        </div>
    </c:if>

    <!-- Body Page -->
    <div class="span-19 last">
        <c:if test="${!empty requestScope.widgets && !empty requestScope.widgets['body_top']}">
            <div class="border" style="background-color:#FCFCFC;">
                <c:forEach var="widget" items="${requestScope.widgets['body_top']}">
                    <tiles:insertDefinition name="${widget.key}"/>
                </c:forEach>
            </div>
        </c:if>
        <c:if test="${!empty requestScope.plugin_page}">
            <tiles:insertDefinition name="${requestScope.plugin_page}"/>
        </c:if>
    </div>

    <!-- Footer Page -->
    <div class="footer span-24">
        <ul style="list-style:none;line-height:28px;">

            <li>
                <spring:url value="/index" var="homeUrl" htmlEscape="true"/>
                <a href="${homeUrl}">Home</a>
            </li>

            <li>
                <spring:url value="/viewPeson" var="personListUrl" htmlEscape="true"/>
                <a href="${personListUrl}">Person List</a>
            </li>
        </ul>
    </div>
</div>
</body>
</html>