<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="widget">
    <tiles:importAttribute name="title" toName="title"/>
    <c:if test="${!empty requestScope.title}">
        <h3 class="widget-title"><tiles:putAttribute name="title"/></h3>
    </c:if>

    <div class="widget-body">
        <tiles:insertAttribute name="body"/>
    </div>
</div>
