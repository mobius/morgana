package com.dyggyty.morgana.persistence;

import com.dyggyty.morgana.model.flow.FlowStatusValue;

/**
 * JPA CRUD repository for the FlowStatusValue entity.
 */
public interface FlowStatusValueCrudRepository extends AttachedEntityCrudRepository<FlowStatusValue> {
}
