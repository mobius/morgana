package com.dyggyty.morgana.persistence;

import com.dyggyty.morgana.model.flow.FlowStatusValue;
import org.springframework.transaction.annotation.Transactional;

/**
 * CRUD repository for the FlowStatusValue entity.
 */
@Transactional
public class FlowStatusValueCrudRepositoryImpl extends GenericAttachedEntityCrudRepository<FlowStatusValue>
        implements FlowStatusValueCrudRepository {
}
