package com.dyggyty.morgana.plugins.flow;

import com.dyggyty.morgana.model.flow.FlowStatusValue;

/**
 * Process entity flow status.
 */
public interface FlowProcessor {

    /**
     * Will be called in main entity update case.
     *
     * @param mainEntityBefore Previous main entity value.
     * @param mainEntityAfter  Current main entity value.
     * @param currantStatus    Current flow value.
     *                         Can be <b>NULL</b> if no status was before update and user does not made any updates.
     * @return New status value.
     */
    FlowStatusValue onEntityUpdate(Object mainEntityBefore, Object mainEntityAfter, FlowStatusValue currantStatus);

    /**
     * Will be called in main entity creation..
     *
     * @param mainEntity    Main entity value.
     * @param currantStatus Can be <b>NULL</b> if user does not made any updates.
     * @return Entity status value.
     */
    FlowStatusValue onEntityCreate(Object mainEntity, FlowStatusValue currantStatus);
}
