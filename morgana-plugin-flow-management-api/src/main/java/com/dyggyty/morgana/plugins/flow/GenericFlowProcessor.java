package com.dyggyty.morgana.plugins.flow;

import com.dyggyty.morgana.model.flow.FlowStatusValue;

/**
 * Common flow processor engine
 */
public class GenericFlowProcessor implements FlowProcessor {
    @Override
    public FlowStatusValue onEntityUpdate(Object mainEntityBefore, Object mainEntityAfter, FlowStatusValue currantStatus) {
        return currantStatus;
    }

    @Override
    public FlowStatusValue onEntityCreate(Object mainEntity, FlowStatusValue currantStatus) {
        return currantStatus;
    }
}
