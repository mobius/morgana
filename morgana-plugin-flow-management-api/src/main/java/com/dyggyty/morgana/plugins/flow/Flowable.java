package com.dyggyty.morgana.plugins.flow;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that entity is flowable
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Flowable {

    /**
     * Flow processor will be called on main entity modification case.
     *
     * @return Flow processor class.
     */
    Class<? extends FlowProcessor> flowProcessor() default GenericFlowProcessor.class;

    /**
     * Indicates can flow be edited manually or not.
     *
     * @return <b>true</b> if flow ca be edited manually and <b>false</b> otherwise.
     */
    boolean editable() default true;
}
