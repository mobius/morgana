package com.dyggyty.morgana.model.flow;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Fow status value.
 */
@Entity
@Table(name = "flow_status_entry")
public class FlowStatusEntry {

    @Id
    @GeneratedValue
    private Long id;

    @Column(length = 60, name = "status_value")
    private String statusValue;

    @Column(length = 4000)
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
