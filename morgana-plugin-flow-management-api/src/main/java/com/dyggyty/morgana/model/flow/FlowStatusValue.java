package com.dyggyty.morgana.model.flow;

import com.dyggyty.morgana.configuration.EntityDescriptor;
import com.dyggyty.morgana.model.AttachedEntity;
import com.dyggyty.morgana.persistence.FlowStatusValueCrudRepositoryImpl;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Represents entity current flow value.
 */
@Entity
@Table(name = "flow_status_value")
@EntityDescriptor(value = "", repositoryClass = FlowStatusValueCrudRepositoryImpl.class)
public class FlowStatusValue extends AttachedEntity {

    @ManyToOne
    private FlowStatusEntry flowStatusEntry;

    public FlowStatusEntry getFlowStatusEntry() {
        return flowStatusEntry;
    }

    public void setFlowStatusEntry(FlowStatusEntry flowStatusEntry) {
        this.flowStatusEntry = flowStatusEntry;
    }
}
